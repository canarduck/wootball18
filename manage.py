#!/usr/bin/env python3
"""
Interface de gestion de wootball

./manage.py initdb
"""

import csv
from datetime import datetime, timedelta

import click
import pytz
import requests
from flask.cli import FlaskGroup
from pays import Countries
from pytz import timezone

from wootball import create_app, db_wrapper
from wootball.models import MODELS, Competition, Game, Team, Forecast
# from wootball.tasks import notify_games, notify_forecast


def update_from_csv(obj: object, row: dict) -> object:
    """
    Met à jour un modèle depuis une ligne csv (ou un dict)
    retourne l'objet modifié
    """
    for attr, value in row.items():
        if attr == 'id':
            continue
        if attr.startswith('is_'):
            value = bool(int(value))
        if getattr(obj, attr) != value:
            setattr(obj, attr, value)
    return obj


cli = FlaskGroup(create_app=create_app)


@click.option('--drop',
              is_flag=True,
              help='Efface les tables avant re-création')
@cli.command('initdb')
def initdb(drop: bool):
    """Initialise la base de données"""
    click.echo('Connexion à la base...')
    db_wrapper.database.connect()
    click.echo('Création des tables...')
    for model in MODELS:
        name = model.__name__
        if drop:
            # sqlite ne supporte pas cascade
            model.drop_table(fail_silently=True, cascade=False)
            click.echo('× table {model} supprimée'.format(model=name))
        if model.table_exists():
            click.echo('✅ table {model} déjà en base'.format(model=name))
        else:
            model.create_table(fail_silently=False)
            click.echo('✓ table {model} créée'.format(model=name))
    click.echo('✔ Terminé !')
    click.echo('Déconnexion...')
    db_wrapper.database.close()


@cli.command('competitions')
def import_competitions():
    """Importe la liste des competitions depuis competitions.csv"""
    click.echo('Connexion à la base...')
    db_wrapper.database.connect()
    click.echo('Import des données...')
    with open('fixtures/competitions.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            competition, created = Competition.get_or_create(
                external_id=row['external_id'], defaults=row)
            if created:
                click.echo(
                    '✅ création {c.name} (#{c.id})'.format(c=competition))
            else:
                competition = update_from_csv(competition, row)
                if competition.is_dirty():
                    competition.save()
                    click.echo('✅ mise à jour {c.name} (#{c.id})'.format(
                        c=competition))
                else:
                    click.echo(
                        '⦾ rien à modifier pour {c.name} (#{c.id})'.format(
                            c=competition))
    click.echo('✔ Terminé !')
    click.echo('Déconnexion...')
    db_wrapper.database.close()


@cli.command('teams')
def import_teams():
    """Importe la liste des équipes depuis teams.csv"""
    click.echo('Connexion à la base...')
    db_wrapper.database.connect()
    click.echo('Import des données...')
    with open('fixtures/teams.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            team, created = Team.get_or_create(external_id=row['external_id'],
                                               defaults=row)
            if created:
                click.echo('✅ création {t.name} (#{t.id})'.format(t=team))
            else:
                team = update_from_csv(team, row)
                if team.is_dirty():
                    team.save()
                    click.echo(
                        '✅ mise à jour {t.name} (#{t.id})'.format(t=team))
                else:
                    click.echo(
                        '⦾ rien à modifier pour {t.name} (#{t.id})'.format(
                            t=team))
    click.echo('✔ Terminé !')
    click.echo('Déconnexion...')
    db_wrapper.database.close()


@cli.command('games')
def import_games():
    """Importe la liste des matchs depuis games.csv"""
    click.echo('Connexion à la base...')
    db_wrapper.database.connect()
    click.echo('Import des données...')
    with open('fixtures/games.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            try:
                game = Game.get(external_id=row['external_id'])
            except Game.DoesNotExist:
                game = Game()
            for attr, value in row.items():
                if attr == 'id':
                    continue
                if attr.startswith('is_'):
                    value = bool(int(value))
                if attr.endswith('_team_id'):
                    team = Team.get(external_id=value)
                    value = team.id
                if attr == 'competition_id':
                    competition = Competition.get(external_id=value)
                    value = competition.id
                if attr == 'date':
                    # HACK ajoute 2h pour l'heure FR
                    # passe value en datetime sinon peewee stocke string
                    dt = datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')
                    value = dt.replace(tzinfo=None) + timedelta(hours=2)
                if value == '':
                    value = None
                if getattr(game, attr) != value:
                    setattr(game, attr, value)
            if game.is_dirty():
                game.save()
                click.echo('✅ mise à jour {g} (#{g.id})'.format(g=game))
            else:
                click.echo(
                    '⦾ rien à modifier pour {g} (#{g.id})'.format(g=game))
    click.echo('✔ Terminé !')
    click.echo('Déconnexion...')
    db_wrapper.database.close()


# @cli.command('notify_games')
# def send_games_notifications():
#     """Notifie les users des prochains matchs"""
#     click.echo('Connexion à la base...')
#     db_wrapper.database.connect()
#     click.echo('Exécution de la tâche...')
#     notify_games()
#     click.echo('✔ Terminé !')
#     click.echo('Déconnexion...')
#     db_wrapper.database.close()

# @cli.command('notify_forecast')
# @click.argument('game_id')
# def send_forecasts_notifications(game_id: int):
#     """Envoi des notifications pour le match game_id
#     """
#     click.echo('Connexion à la base...')
#     db_wrapper.database.connect()
#     click.echo('Récupération des pronos...')
#     forecasts = Forecast.select().join(Game).where(
#         (Forecast.notified_at.is_null(True)) &
#         (Game.id == game_id))  # pylint: disable E1101
#     for forecast in forecasts:
#         user = forecast.user
#         click.echo('Envoi à {}'.format(user))
#         notify_forecast(forecast.id)
#     click.echo('✔ Terminé !')
#     click.echo('Déconnexion...')
#     db_wrapper.database.close()

if __name__ == '__main__':
    cli()
