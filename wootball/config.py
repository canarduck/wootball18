"""
Configurations de wootball selon les contextes
Pour modifier la configuration utiliser la variable d'environnement FLASK_CONFIG
cf. wootball/__init__.py
"""
import logging
import os


DATABASE_URL = "sqliteext+pool:///databases/{}.db"


class BaseConfig(object):
    """
    Paramètres par défaut
    """

    CONFIG_NAME = "base"
    DEBUG = False
    TESTING = False
    DATABASE = "sqlite:///:memory:"
    SECRET_KEY = os.environ.get("SECRET_KEY")
    SECURITY_PASSWORD_SALT = os.environ.get("SECURITY_PASSWORD_SALT")
    LOGGING_LEVEL = logging.ERROR
    LOGGING_FILE = "disques.log"
    UPLOAD_FOLDER = "wootball/static/uploads"
    VERSION = "0.9.4"  # géré par bumpversion
    CURRENT_WOMPIL = "2018"
    CURRENT_COMPETITION = 500
    BASE_URL = "https://wootball.fr"
    MAIL_DEFAULT_SENDER = "Wootball <wootball@wootball.fr>"
    # https://flask-security.readthedocs.io/en/latest/configuration.html
    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = True
    SECURITY_CONFIRMABLE = False
    SECURITY_CHANGEABLE = True
    LOGIN_WITHOUT_CONFIRMATION = True
    BABEL_DEFAULT_LOCALE = "fr"
    SECURITY_LOGIN_URL = "/bijour"
    SECURITY_LOGOUT_URL = "/boujou"
    SECURITY_RESET_URL = "/risette"
    SECURITY_CHANGE_URL = "/change-moi"
    SECURITY_CONFIRM_URL = "/je-viens"
    SECURITY_REGISTER_URL = "/viens"
    SECURITY_EMAIL_SUBJECT_REGISTER = "Inscription à wootball"
    SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE = "Mot de passe risetté"
    SECURITY_EMAIL_SUBJECT_PASSWORD_RESET = "Risette du mot de passe"
    SECURITY_EMAIL_SUBJECT_PASSWORD_CHANGE_NOTICE = "Mot de passe modifié"
    SECURITY_EMAIL_SUBJECT_CONFIRM = "Email à confirmer"
    SECURITY_DEFAULT_REMEMBER_ME = True
    SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL = False
    SECURITY_POST_REGISTER_VIEW = "/pronos"
    SECURITY_POST_LOGIN_VIEW = "/pronos"
    SECURITY_POST_LOGOUT_VIEW = SECURITY_POST_LOGIN_VIEW
    SECURITY_USER_IDENTITY_ATTRIBUTES = ["email", "username"]
    AVAILABLE_LANGUAGES = ["fr", "ru"]
    DB_NAME = "wootball.db"

    DATABASE = DATABASE_URL.format("wootball")


class ProductionConfig(BaseConfig):
    """
    Réglages en production
    """

    CONFIG_NAME = "prod"
    LOGGING_LEVEL = logging.WARN
    SERVER_NAME = "wootball.fr"


class DevelopmentConfig(BaseConfig):
    """
    Réglages en développement
    """

    CONFIG_NAME = "dev"
    DEBUG = True
    SECRET_KEY = "taupe/secret"
    SECURITY_PASSWORD_SALT = "taupe/secret"
    DATABASE = DATABASE_URL.format("wootball_dev")
    LOGGING_LEVEL = logging.DEBUG
    SERVER_NAME = "localhost:3000"


class TestingConfig(BaseConfig):
    """
    Réglages en test / CI
    """

    CONFIG_NAME = "testing"
    TESTING = True
    SECRET_KEY = "taupe/secret"
    SECURITY_PASSWORD_SALT = "taupe/secret"
    SERVER_NAME = "localhost.localdomain:5000"
    # les tests en :memory: échouent
    # cf. https://github.com/coleifer/peewee/issues/193
    DATABASE = DATABASE_URL.format("wootball_test2")
    WTF_CSRF_ENABLED = False
    LOGGING_LEVEL = logging.INFO
