"""
prono wootball

"""
# pylint: disable=E1120
import random
import re

import oyaml as yaml
from flask import (
    Blueprint,
    Markup,
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    session,
    url_for,
    request,
)
from flask_security import current_user, login_required
from peewee import PeeweeException
from playhouse.flask_utils import get_object_or_404, object_list

from wootball.forms import ForecastCardForm, GroupForm
from wootball.models import Forecast, Game, Group, Team, User
from wootball.utils import save_upload

prono = Blueprint("prono", __name__)  # pylint: disable=C0103


@prono.route("/")
def index():
    """L’accueil des pronostics."""
    forms = []
    for game in Game.forthcomings():
        if current_user.is_authenticated:
            try:
                forecast = Forecast.get(user_id=current_user.id, game=game)
            except Forecast.DoesNotExist:
                forecast = Forecast(user_id=current_user.id, game=game)
            form = ForecastCardForm(obj=forecast)

            if game.question:
                form.answer.choices = [("", "Pas de réponse")] + [
                    (answer, answer) for answer in game.answer_choices_list
                ]
            else:
                form.answer = ""
            if game.phase != "pools":
                form.shoot_out_victory.blank_text = "-- pas de tirs aux buts --"
                form.shoot_out_victory.choices = [
                    ("home", game.home_team),
                    ("away", game.away_team),
                ]
            form._id = forecast.id  # conserver l’id du pari
            form._locked = forecast.locked  # conserver l’id du pari
            forms.append(form)

    return render_template(
        "pronos.html",
        user_count=User.select().count(),
        games=Game.forthcomings(),
        teams=Team.select(),
        ranks=User.select()
        .where(User.active == True)
        .order_by(User.score.desc())
        .limit(12),
        ranks_finals=User.select()
        .where((User.active == True) & (User.score_finals > 0))
        .order_by(User.score_finals.desc())
        .limit(12),
        forms=forms,
    )


@prono.route("/pronostiquer/<int:game_id>", methods=["POST"])
@login_required
def forecast_form(game_id: int):
    """
    Soumission d’un prono
    """
    game = get_object_or_404(Game.forthcomings(), (Game.id == game_id))
    if game.closed:
        flash(
            "Beh non, on touche pas aux prono une fois le match commencé / passé, petit coquing’",
            "error",
        )
        return redirect(url_for(".index"))
    try:
        forecast = Forecast.get(user_id=current_user.id, game=game)
    except Forecast.DoesNotExist:
        forecast = Forecast(user_id=current_user.id, game=game)
    form = ForecastCardForm(obj=forecast)
    form.shoot_out_victory.validators = []  # FIXME
    if game.question:  # TODO refactoriser
        form.answer.choices = [("", "Pas de réponse")] + [
            (answer, answer) for answer in game.answer_choices_list
        ]

    if form.validate_on_submit():
        form.populate_obj
        form.populate_obj(forecast)
        if not game.question:
            forecast.answer = None
        try:
            pk = forecast.id
            forecast.save()
            if pk:
                flash(
                    "Pronostic {home} / {away} mis à jour".format(
                        home=forecast.game.home_team.name,
                        away=forecast.game.away_team.name,
                    ),
                    "success",
                )
            else:
                flash(
                    "Pronostic {home} / {away} créé (il peut encore être modifié ou annulé)".format(
                        home=forecast.game.home_team.name,
                        away=forecast.game.away_team.name,
                    ),
                    "success",
                )
        except PeeweeException as e:
            flash("Erreur lors de la sauvegarde : {e}".format(e=e), "error")
    elif form.is_submitted():
        flash("Erreurs à corriger dans le formulaire", "error")
    return redirect(url_for(".index"))


@prono.route("/annuler/<int:pk>")
@login_required
def forecast_cancel(pk: int):
    """
    Annulation d’un prono
    """
    try:
        forecast = Forecast.get(user_id=current_user.id, id=pk)
    except Forecast.DoesNotExist:
        abort(404)
    if forecast.locked:
        flash(
            "Pronostic verrouillé (match entamé ?), impossible de l’annuler", "warning"
        )
    else:
        flash(
            "Pronostic {home} / {away} annulé".format(
                home=forecast.game.home_team.name, away=forecast.game.away_team.name
            ),
            "warning",
        )
        forecast.delete_instance()

    return redirect(url_for(".index"))


@prono.route("/archives")
def archives():
    """
    Archives des matches
    """
    games = Game.published().order_by(Game.date.desc())

    return object_list(
        "archives.html",
        query=games,
        context_variable="object_list",
        paginate_by=25,
        count=games.count(),
    )


@prono.route("/classements/poules")
def ranks():
    """
    Classement des pronostiqueurs phase poules
    """
    users = User.select().where(User.active == True).order_by(User.score.desc())

    return object_list(
        "ranks.html",
        query=users,
        context_variable="object_list",
        paginate_by=30,
        phase="pools",
        count=users.count(),
    )


@prono.route("/classements/finales")
def ranks_finals():
    """
    Classement des pronostiqueurs phases finales
    """
    users = User.select().where(User.active == True).order_by(User.score_finals.desc())

    return object_list(
        "ranks.html",
        query=users,
        context_variable="object_list",
        paginate_by=30,
        phase="finals",
        count=users.count(),
    )


@prono.route("/stats/pointures.js")
def stats_shoe_sizes():
    "Pointure moyenne des users"
    shoe_sizes = User.stats_shoe_sizes()
    return render_template("stats/pointures.js", values=shoe_sizes)


@prono.route("/stats/dep_scores.js")
def stats_dep_scores():
    "Points par département"
    dep_scores = User.stats_dep_scores()
    return render_template("stats/dep_scores.js", values=dep_scores)


@prono.route("/stats/likes.js")
def stats_likes():
    "Points par département"
    likes = User.stats_likes()
    return render_template("stats/likes.js", values=likes)


@prono.route("/u/<int:pk>/<string:slug>")
def user_detail(pk: int, slug: str):
    """
    Détail d'un user
    """
    active_users = User.select().where(User.active == True)
    user = get_object_or_404(active_users, ((User.id == pk) & (User.slug == slug)))
    return render_template(
        "user.html",
        object=user,
        user_count=User.select().count(),
    )


@prono.route("/g/<int:pk>/<string:slug>")
def group_detail(pk: int, slug: str):
    """
    Détail d'un groupe
    """
    group = get_object_or_404(Group.select(), ((Group.id == pk) & (Group.slug == slug)))
    return render_template(
        "group.html", object=group, group_count=Group.select().count()
    )


@prono.route("/t/<int:pk>/<string:slug>")
def team_detail(pk: int, slug: str):
    """
    Détail d'une équipe
    """
    team = get_object_or_404(Team.select(), ((Team.id == pk) & (Team.slug == slug)))
    return render_template("team.html", object=team)


@prono.route("/m/<string:slug>")
def game_detail(slug: str):
    """
    Détail d'un match
    """
    game = get_object_or_404(Game.select(), (Game.slug == slug))
    return render_template("game.html", object=game)


@prono.route("/groupe", methods=["GET", "POST"])
@login_required
def group_form():
    """
    Formulaire de création / modification de groupe
    """
    user = User.get(User.id == current_user.id)
    group = user.group or Group()
    form = GroupForm(obj=group)
    if form.validate_on_submit():
        new = True
        form.populate_obj(group)
        if group.id:
            new = True
        try:
            group.save()
        except PeeweeException:
            form.name.errors.append("Un groupe s’appelle déjà comme ça")
            return render_template("group_form.html", form=form, object=group)
        upload = form.new_avatar.data
        avatar = save_upload(upload, "avatars", ["g", str(group.id), group.slug])
        if avatar:
            group.avatar = avatar
            group.save()

        if new:
            user.group = group
            user.save()
            message = "Groupe créé"
        else:
            message = "Groupe mis à jour"
        flash(
            Markup(
                '{message}, pour y inviter les copaings : <a href="{url}">{url}</a>'.format(
                    url=group.get_invitation_url, message=message
                )
            ),
            "success",
        )
        return redirect(url_for("prono.index"))
    return render_template("group_form.html", form=form, object=group)


@prono.route("/groupe/boujou")
@login_required
def group_quit():
    """
    Quittage de groupe
    """
    user = User.get(User.id == current_user.id)
    group = user.group
    if not group:
        abort(500)
    user.group = None
    user.save()
    if group.users.count() == 0:  # si plus personne on efface le groupe
        group.delete_instance()
        flash(
            "Le groupe {group} n’est plus et {user} est dorénavant seul sur le terrain".format(
                group=group.name, user=current_user.username
            ),
            "info",
        )
    else:
        group.save()  # update du score
        flash(
            "{user} a quitté {group} et se promène seul sur le terrain".format(
                group=group.name, user=current_user.username
            ),
            "info",
        )
    return redirect(url_for(".index"))


@prono.route("/invitation/<int:pk>/<string:code>")
def group_invitation(pk: int, code: str):
    """
    Formulaire de création / modification de groupe
    Si pas authentifié on crée la variable de session group_invitation

    """
    try:
        group = Group.get(Group.id == pk)
    except Group.DoesNotExist:
        abort(404)  # TODO améliorer le message
    if group.code != code:
        abort(404)  # TODO améliorer le message
    if not current_user.is_authenticated:
        session["group_invitation"] = group.id
        flash("Il faut être authentifié pour rejoindre un groupe", "info")
        url = url_for(".group_invitation", pk=group.id, code=group.code)
        redirect(current_app.config["SECURITY_LOGIN_URL"] + "?next=" + url)
    if current_user.group and current_user.group.id == group.id:
        flash(
            "{user} est déjà membre du groupe {group}".format(
                group=group.name, user=current_user.username
            ),
            "warning",
        )
        return redirect(url_for(".index"))
    elif current_user.group:
        flash(
            "{user} est déjà membre du groupe {current}, il faut les quitter avant de pouvoir rejoindre les {new}".format(
                current=current_user.group.name,
                new=group.name,
                user=current_user.username,
            ),
            "warning",
        )
        return redirect(url_for(".index"))
    else:
        current_user.group = group
        current_user.save()
        flash("Ayé, vous êtes membre de {name}".format(name=group.name), "success")
    return render_template("group_invitation.html", object=group)


@prono.route("/desabonnement/<int:pk>/<string:code>")
def user_unsubscribe(pk: int, code: str):
    """
    Formulaire de création / modification de groupe
    Si pas authentifié on crée la variable de session group_invitation
    """
    try:
        user = User.get(User.id == pk)
    except User.DoesNotExist:
        abort(404)  # TODO améliorer le message
    if user.code != code:
        abort(404)  # TODO améliorer le message
    user.mailable = False
    user.save()
    flash(
        "Roger ça, {user} ne recevra plus les mails liés aux pronostics (relances et résultats)".format(
            user=current_user.username
        ),
        "info",
    )
    return redirect(url_for(".index"))
