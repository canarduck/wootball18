{% extends 'stats/base.js' %}

{% set var_name = 'statsDepScore' %}
{% set title = 'Points par département' %}
{% set chart_type = 'line' %}
{% block extra_legend %}display: false{% endblock %}

{% block labels -%}
{% for dep, score in values %}'{{ dep }}', {% endfor %}
{%- endblock %}
{% block data -%}
{% for dep, score in values %}{{ score }}, {% endfor %}
{%- endblock %}