{% extends 'stats/base.js' %}

{% set var_name = 'statsPointures' %}
{% set title = 'Pointures des pronostiqueurs' %}
{% set chart_type = 'bar' %}
{% block extra_legend %}display: false{% endblock %}

{% block labels -%}
{% for size, count in values %}{{ size }}, {% endfor %}
{%- endblock %}
{% block data -%}
{% for size, count in values %}{{ count }}, {% endfor %}
{%- endblock %}