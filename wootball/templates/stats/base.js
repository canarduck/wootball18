
var ctx = document.getElementById('canvas_{{ request.args.c|int(1) }}').getContext('2d');
var {{ var_name }} = new Chart(ctx, {
    type: '{{ chart_type }}',

    // The data for our dataset
    data: {
        labels: [{% block labels %}{% endblock %}],
        {% block datasets %}
        datasets: [{
            data: [{% block data %}{% endblock %}],
            backgroundColor: [{% block colors %}
                {% for _ in values %}'rgba({{ range(0, 255) | random }}, {{ range(0, 255) | random }}, {{ range(0, 255) | random }}, 0.8)',
                {% endfor %}{% endblock colors %}]
        }]
        {% endblock datasets %}
    },
    {% block options %}
    options: {
        title: {
            display: true,
            text: '{{ title }}',
            fontFamily: 'FaricyNew',
            fontSize: 24,
            position: 'bottom',
            fontColor: 'rgb(255, 255, 255)'
        },
        legend: {
            labels: {
                fontColor: 'rgb(255, 255, 255)'
            },
            {% block extra_legend %}{% endblock %}
        }
    }
    {% endblock options %}
});