{% extends 'stats/base.js' %}

{% set var_name = 'statsLikes' %}
{% set title = 'Amour du foot' %}
{% set chart_type = 'doughnut' %}
{% block extra_legend %}display: false{% endblock %}

{% block labels -%}
'Aime', 'Aime pas'
{%- endblock %}
{% block data -%}
{{ values.0 }}, {{ values.1 }}
{%- endblock %}