/**
__      _____   ___ | |_| |__   __ _| | | 
\ \ /\ / / _ \ / _ \| __| '_ \ / _` | | | 
 \ V  V / (_) | (_) | |_| |_) | (_| | | | 
  \_/\_/ \___/ \___/ \__|_.__/ \__,_|_|_|.js
*/

document.addEventListener("DOMContentLoaded", function (event) {

    // liens rel=external -> target=_blank 
    document.querySelectorAll("a[rel='external']").forEach(function (anchor) {
        anchor.setAttribute('target', '_blank');
    })

    // Menu responsive
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    if ($navbarBurgers.length > 0) {
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {
                var target = $el.dataset.target;
                var $target = document.getElementById(target);
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }

    // boutons à confirmer
    document.querySelectorAll('.has-confirmation').forEach(function (button) {
        button.addEventListener('click', function (event) {
                event.preventDefault();
                if (confirm('Sûr ?')) {
                    window.location = this.href;
                }
            },
            false);
    });

    // Lecteur mp3
    var tracklist = [];
    var playlist = [];
    var kick;

    document.querySelectorAll('tr.track').forEach(function (tr) {
        if (tr.getAttribute('data-file')) {
            tracklist.push(tr.getAttribute('data-file'));
        } else {
            tracklist.push(null)
        }
    });
    soundManager.setup({
        preferFlash: false,
        onready: function () {
            tracklist.forEach(function (url, i) {
                playlist[i] = soundManager.createSound({
                    url: url
                });
            });
            kick = soundManager.createSound({
                url: "/static/audio/kick.mp3"
            });
        }
    });
    document.querySelectorAll('.play-button').forEach(function (button) {
        button.addEventListener('click', function (event) {
            let sound = playlist[button.getAttribute('data-track')];
            if (sound.playState) {
                sound.stop();
                button.innerHTML = '▶️';
            } else {
                playlist.forEach(function (track, i) {
                    track.stop()
                    document.querySelector('button[data-track="' + i + '"]').innerHTML = '▶️';
                })
                sound.play();
                button.innerHTML = '⏹️';
            }
        });
    });
    document.querySelectorAll('a').forEach(function (anchor) {
        anchor.addEventListener('mouseenter', function (event) {
            kick.play();
        });
    });

    // slideshows
    var currentSlide = 1;
    var currentPoints = 0;
    var quiz = document.getElementById('quiz');

    document.querySelectorAll('.slidecontrol').forEach(function ($btn) {
        $btn.addEventListener('click', function () {
            let newSlide = $btn.getAttribute('data-slide');
            let points = $btn.getAttribute('data-points');
            if (points) {
                currentPoints += parseInt(points);
            }
            if (newSlide == 'results' && quiz) {
                if (currentPoints == parseInt(quiz.getAttribute('data-slides'))) {
                    window.location.replace(quiz.getAttribute('data-win-url'));
                } else if (currentPoints == 0) {
                    window.location.replace(quiz.getAttribute('data-fail-url'));
                } else {
                    window.location.replace(quiz.getAttribute('data-lose-url'));
                }
            } else {
                document.getElementById('slide-' + currentSlide).classList.toggle('is-hidden');
                document.getElementById('slide-' + newSlide).classList.toggle('is-hidden');
                currentSlide = newSlide;
            }

        });
    });


})