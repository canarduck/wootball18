"""Wootball."""
import locale
import logging
import logging.handlers
import os
from datetime import datetime

from flask import (
    Flask,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    session,
)
from flask_babelex import Babel
from flask_mail import Mail
from flask_security import Security, current_user, login_required
from flask_wtf.csrf import CSRFProtect

from slugify import slugify
from werkzeug.utils import secure_filename

from wootball.admin import admin
from wootball.filters import (
    premailer_transform,
    strftime_filter,
    signed_number,
    markdown_filter,
    slugify_filter,
)
from wootball.forms import ConfirmRegisterForm, ProfileForm, RegisterForm
from wootball.jeux import jeux
from wootball.models import User, db_wrapper, user_datastore
from wootball.prono import prono
from wootball.utils import save_upload
from wootball.wompil import wompil

# pylint: disable=C0103
csrf = CSRFProtect()
mail = Mail()
security = Security()
babel = Babel()

try:
    locale.setlocale(locale.LC_ALL, "fr_FR.utf8")
except locale.Error:
    pass


def handle_400(e=None):
    """Bad request."""
    current_app.logger.error(e)
    return render_template("400.html", exception=e), 400


def handle_404(e=None):
    """Not found."""
    current_app.logger.error(e)
    return render_template("404.html", exception=e), 404


def handle_500(e=None):
    """Internal error."""
    current_app.logger.error(e)
    return render_template("500.html", exception=e), 500


def homepage():
    """Page d'accueil."""
    return render_template("homepage.html")


def pouce():
    """Page intermédiaire quand on attend une confirmation.

    ex. post register, post reset mdp...
    """
    return render_template("security/pouce.html")


def landing():
    """Page d’accueil temporaire."""
    return render_template("landing.html")


@login_required
def profile():
    """Modification du profil."""
    user = User.get(User.id == current_user.id)
    user_profile_form = ProfileForm(obj=user)
    if user_profile_form.validate_on_submit():
        user_profile_form.populate_obj(user)
        user.save()
        upload = user_profile_form.new_avatar.data
        avatar = save_upload(upload, "avatars", ["u", str(user.id), user.slug])
        if avatar:
            user.avatar = avatar
            user.save()
        flash("Profil mis à jour", "success")
        return redirect(url_for("prono.index"))
    return render_template("security/profile.html", user_profile_form=user_profile_form)


def change_language():
    """Passe en session la langue demandée

    GET ?lang=XX
    récupération avec session.get('lang')
    """
    if not session.get("lang"):
        session["lang"] = current_app.config.get("BABEL_DEFAULT_LOCALE")
    if request.args.get("lang"):
        session["lang"] = (
            request.args.get("lang")
            if request.args.get("lang") in current_app.config.get("AVAILABLE_LANGUAGES")
            else current_app.config.get("BABEL_DEFAULT_LOCALE")
        )
        current_app.logger.info("Passage en langue " + session["lang"])


def create_app(config_name="prod"):
    """Application factory."""
    app = Flask(__name__)
    config = {
        "dev": "wootball.config.DevelopmentConfig",
        "testing": "wootball.config.TestingConfig",
        "prod": "wootball.config.ProductionConfig",
    }[os.getenv("FLASK_CONFIG", config_name)]
    app.config.from_object(config)
    app.config.from_pyfile("secrets.cfg", silent=True)
    mode = app.config["CONFIG_NAME"].upper()
    app.logger.info(f"Wootball lancé en mode {mode}")

    # Logging
    app.logger.setLevel(app.config.get("LOGGING_LEVEL"))
    handler = logging.handlers.RotatingFileHandler(
        "logs/debug.log", maxBytes=1024 * 1024
    )
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)

    # Flask-WTF
    csrf.init_app(app)

    # Flask-Mail
    mail.init_app(app)

    # BDD
    db_wrapper.init_app(app)
    # db_wrapper.database.pragma('foreign_keys', 'on')
    # db_wrapper.database.close()

    # Babel
    babel.init_app(app)

    # Security
    security.init_app(
        app,
        user_datastore,
        register_form=RegisterForm,
        confirm_register_form=ConfirmRegisterForm,
    )

    # pylint: disable=W0612
    # @security_ctx.send_mail_task
    # def delay_flask_security_mail(msg):
    #     "envoi des mails security"
    #     flask_security_mail(  # sans sender qui fait planter huey (pickle lambda)
    #         subject=msg.subject,
    #         recipients=msg.recipients,
    #         body=msg.body,
    #         html=msg.html)

    # pylint: enable=W0612

    # Pages d’erreur
    app.register_error_handler(400, handle_400)
    app.register_error_handler(404, handle_404)
    app.register_error_handler(500, handle_500)

    # Pages simples
    app.add_url_rule("/", "homepage", view_func=homepage)
    app.add_url_rule("/pouce", "pouce", view_func=pouce)
    app.add_url_rule("/landing", "landing", view_func=landing)

    # Pages utilisateur
    app.add_url_rule("/profil", "profile", view_func=profile, methods=("GET", "POST"))

    # Blueprints
    app.register_blueprint(wompil, url_prefix="/wompil")
    app.register_blueprint(prono, url_prefix="/pronos")
    app.register_blueprint(jeux, url_prefix="/jeux")
    app.register_blueprint(admin, url_prefix="/coach")

    app.before_request(change_language)

    # Templates tags
    # pylint: disable=E1101
    app.jinja_env.filters["premailer"] = premailer_transform
    app.jinja_env.filters["date"] = strftime_filter
    app.jinja_env.filters["signed"] = signed_number
    app.jinja_env.filters["markdown"] = markdown_filter
    app.jinja_env.filters["slugify"] = slugify_filter

    return app
