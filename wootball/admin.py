"""
Admin wootball
"""
# pylint: disable=E1120
import os
from datetime import datetime

from flask import Blueprint, current_app, flash, redirect, render_template, url_for
from flask_security import login_required, roles_required
from flask_mail import Mail, Message
from peewee import PeeweeException
from playhouse.flask_utils import get_object_or_404, object_list
from slugify import slugify
from werkzeug.utils import secure_filename

from wootball.forms import ForecastForm, GameForm, GroupForm, TeamForm, UserForm
from wootball.models import Competition, Forecast, Game, Group, Team, User

# from wootball.tasks import score_forecasts_for_game

# pylint: disable=C0103
admin = Blueprint("admin", __name__, template_folder="templates/admin")

# Pagination des listes
PAGINATE_BY = 50

mail = Mail()

# def score_forecasts():
#     """
#     Recherche de match terminé à scorer toutes les 30 minutes
#     """
#     games = Game.select().where((Game.scored_at.is_null(True)) &
#                                 (Game.done == True))
#     for game in games:
#         current_app.logger.info(
#             'Trouvé match {game} à scorer'.format(game=game))
#         score_forecasts_for_game(game.id)


def score_forecasts_for_game(game_id: int) -> list:
    """
    Scoring des pronostics liés au Match game_id
    On recalcule complètement les scores des users qui ont pronostiqué histoire
    d'éviter les soucis de race conditions avec huey
    """

    try:
        game = Game.get(id=game_id, done=True)
    except Game.DoesNotExist:
        current_app.logger.error(
            "Demande de scoring du match {id} alors qu’il n’existe pas / ou pas finalisé".format(
                id=game_id
            )
        )
        return []
    msg = "Scoring du match {game} #{id}".format(game=game, id=game_id)
    current_app.logger.info(msg)
    users = []  # liste des users touchés par l’update
    forecasts = []  # liste des forecasts touchés par l’update
    with mail.connect() as conn:
        for forecast in game.forecasts:
            current_app.logger.info(
                "Scoring prono {forecast}".format(forecast=forecast)
            )
            forecast.calculate_scores()
            user = forecast.user
            game = forecast.game
            if forecast.notified_at is None:
                if user.mailable:
                    msg = "Envoi des résultats du pronostic {f} à {f.user}".format(
                        f=forecast
                    )
                    current_app.logger.info(msg)
                    body = render_template(
                        "email/forecast_result.txt",
                        user=user,
                        game=game,
                        forecast=forecast,
                    )
                    html = render_template(
                        "email/forecast_result.html",
                        user=user,
                        game=game,
                        forecast=forecast,
                    )
                    subject = "Résultat du prono {home} / {away}".format(
                        home=game.home_team, away=game.away_team
                    )
                    msg = Message(
                        recipients=[(user.username, user.email)],
                        body=body,
                        html=html,
                        subject=subject,
                    )
                    conn.send(msg)
                else:
                    msg = "{} ne veut pas de mails".format(user)
                    current_app.logger.info(msg)
                forecast.notified_at = datetime.now()
                forecast.save()
            else:
                msg = "Pronostic {} déjà envoyé".format(forecast.id)
                current_app.logger.info(msg)
            users.append(forecast.user.id)
            forecasts.append(forecast.id)
    for user in User.select().where(User.id << users):
        current_app.logger.info("Mise à jour score {user}".format(user=user))
        user.update_score()
    game.scored_at = datetime.now()
    game.save()
    return forecasts


@admin.route("/")
@login_required
@roles_required("coach")
def dashboard():
    """
    Tableau de bord
    """
    return render_template(
        "dashboard.html",
        count_users=User.select().count(),
        last_users=User.select().order_by(User.created_at.desc()).limit(10),
        last_groups=Group.select().order_by(Group.created_at.desc()).limit(10),
        top_users=User.select().order_by(User.score.desc()).limit(10),
        count_games=Game.select().count(),
        draft_games=Game.select()
        .where((Game.draft == True))
        .order_by(Game.date)
        .limit(25),
        published_games=Game.select()
        .where((Game.draft == False) & (Game.done == False))
        .order_by(Game.date)
        .limit(25),
        done_games=Game.select()
        .where((Game.done == True))
        .order_by(Game.date)
        .limit(25),
    )


@admin.route("/équipes")
@login_required
@roles_required("coach")
def teams():
    "Liste des équipes"
    teams = Team.select().order_by(Team.country)

    return object_list(
        "teams.html",
        query=teams,
        context_variable="object_list",
        paginate_by=PAGINATE_BY,
        count=Team.select().count(),
    )


@admin.route("/matches")
@login_required
@roles_required("coach")
def games():
    "Liste des matchs"
    games = Game.select().order_by(Game.date.desc())

    return object_list(
        "games.html",
        query=games,
        context_variable="object_list",
        paginate_by=PAGINATE_BY,
        count=Game.select().count(),
    )


@admin.route("/match/<int:pk>/calcul")
@login_required
@roles_required("coach")
def score_forecasts(pk: int):
    "Calcul des points des pronos du match"
    game = get_object_or_404(Game.select(), (Game.id == pk))
    if not game.done:
        flash("Impossible de scorer un match non terminé", "error")
    else:
        forecasts = score_forecasts_for_game(game.id)
        pronos = ", ".join(map(str, forecasts))
        msg = "Pronostics {} scorés et résultats envoyés".format(pronos)
        flash(msg, "success")
    return redirect(url_for(".games"))


@admin.route("/groupes")
@login_required
@roles_required("coach")
def groups():
    "Liste des groupes"
    groups = Group.select().order_by(Group.name)

    return object_list(
        "groups.html",
        query=groups,
        context_variable="object_list",
        paginate_by=PAGINATE_BY,
        count=Group.select().count(),
    )


@admin.route("/groupes/<int:pk>", methods=["GET", "POST"])
@admin.route("/groupes/_new", methods=["GET", "POST"])
@login_required
@roles_required("coach")
def group_form(pk: int = None):
    "Edition des groupes"
    if pk:
        obj = get_object_or_404(Group.select(), (Group.id == pk))
    else:
        obj = Group()
    form = GroupForm(obj=obj)
    back = url_for(".groups")

    if form.validate_on_submit():
        form.populate_obj(obj)
        try:
            obj.save()
            if pk:
                flash("Groupe {name} mis à jour".format(name=obj.name), "success")
            else:
                flash("Groupe {name} créé".format(name=obj.name), "success")
            return redirect(back)
        except PeeweeException as e:
            flash("Erreur lors de la sauvegarde : {e}".format(e=e), "error")
    elif form.is_submitted():
        flash("Erreurs à corriger dans le formulaire", "error")
    return render_template("admin/group_form.html", object=obj, form=form, back=back)


@admin.route("/équipes/<int:pk>", methods=["GET", "POST"])
@admin.route("/équipes/_new", methods=["GET", "POST"])
@login_required
@roles_required("coach")
def team_form(pk: int = None):
    "Edition des équipes"
    if pk:
        obj = get_object_or_404(Team.select(), (Team.id == pk))
    else:
        obj = Team()
    form = TeamForm(obj=obj)
    back = url_for(".teams")

    if form.validate_on_submit():
        form.populate_obj(obj)
        try:
            obj.save()
            if pk:
                flash("Équipe {name} mise à jour".format(name=obj.name), "success")
            else:
                flash("Équipe {name} créée".format(name=obj.name), "success")
            return redirect(back)
        except PeeweeException as e:
            flash("Erreur lors de la sauvegarde : {e}".format(e=e), "error")
    elif form.is_submitted():
        flash("Erreurs à corriger dans le formulaire", "error")
    return render_template("admin/team_form.html", object=obj, form=form, back=back)


@admin.route("/matches/<int:pk>", methods=["GET", "POST"])
@admin.route("/matches/_new", methods=["GET", "POST"])
@login_required
@roles_required("coach")
def game_form(pk: int = None):
    "Edition des matches"
    if pk:
        obj = get_object_or_404(Game.select(), (Game.id == pk))
    else:
        obj = Game(
            competition=Competition.get(
                external_id=current_app.config["CURRENT_COMPETITION"]
            ),
            home_team=1,
            away_team=1,
        )
    form = GameForm(obj=obj)
    form.shoot_out_victory.validators = []
    if obj.id:
        form.shoot_out_victory.choices = [
            ("home", obj.home_team),
            ("away", obj.away_team),
        ]
    back = url_for(".games")

    if form.validate_on_submit():
        form.populate_obj(obj)

        try:
            obj.save()
            if pk:
                flash("Match {name} mis à jour".format(name=obj), "success")
            else:
                flash("Match {name} créé".format(name=obj), "success")
            return redirect(back)
        except PeeweeException as e:
            flash("Erreur lors de la sauvegarde : {e}".format(e=e), "error")
    elif form.is_submitted():
        flash("Erreurs à corriger dans le formulaire", "error")
    return render_template("game_form.html", object=obj, form=form, back=back)


@admin.route("/utilisateurs")
@login_required
@roles_required("coach")
def users():
    "Liste des utilisateurs"
    query = User.select().order_by(User.username)

    return object_list(
        "users.html",
        query=query,
        context_variable="object_list",
        paginate_by=PAGINATE_BY,
        count=User.select().count(),
    )


@admin.route("/utilisateurs/<int:pk>", methods=["GET", "POST"])
@admin.route("/utilisateurs/_new", methods=["GET", "POST"])
@login_required
@roles_required("coach")
def user_form(pk: int = None):
    "Edition des équipes"
    if pk:
        obj = get_object_or_404(User.select(), (User.id == pk))
    else:
        obj = User()
    form = UserForm(obj=obj)
    back = url_for(".users")

    if form.validate_on_submit():
        form.populate_obj(obj)
        # si maj du mot de passe
        if form.new_password_hash():
            obj.password = form.new_password_hash()
        # si maj de l’avatar
        avatar = form.change_avatar.data
        if hasattr(avatar, "filename"):
            _, ext = os.path.splitext(avatar.filename)
            filename = secure_filename(slugify(obj.username) + ext)
            avatar.save(
                os.path.join(current_app.config["UPLOAD_FOLDER"], "avatars", filename)
            )
            obj.avatar = os.path.join("uploads", "avatars", filename)
        try:
            obj.save()
            if pk:
                flash(
                    "Utilisateur {name} mis à jour".format(name=obj.username), "success"
                )
            else:
                flash("Utilisateur {name} créé".format(name=obj.username), "success")
            return redirect(back)
        except PeeweeException as e:
            flash("Erreur lors de la sauvegarde : {e}".format(e=e), "error")
            flash(form.errors, "error")
            flash(form.shoot_out_victory.validators, "error")
    elif form.is_submitted():
        flash("Erreurs à corriger dans le formulaire", "error")
    return render_template("user_form.html", object=obj, form=form, back=back)


@admin.route("/pronostics")
@login_required
@roles_required("coach")
def forecasts():
    "Liste des pronostics"
    query = Forecast.select().order_by(Forecast.created_at.desc())

    return object_list(
        "forecasts.html",
        query=query,
        context_variable="object_list",
        paginate_by=PAGINATE_BY,
        check_bounds=False,
        count=Forecast.select().count(),
    )


@admin.route("/pronostics/<int:pk>", methods=["GET", "POST"])
@admin.route("/pronostics/_new", methods=["GET", "POST"])
@login_required
@roles_required("coach")
def forecast_form(pk: int = None):
    "Edition des pronostics"
    if pk:
        obj = get_object_or_404(Forecast.select(), (Forecast.id == pk))
    else:
        obj = Forecast(game=Game.select().first(), user=User.select().first())
    form = ForecastForm(obj=obj)
    back = url_for(".forecasts")

    if form.validate_on_submit():
        form.populate_obj(obj)
        try:
            obj.save()
            if pk:
                flash("Pronostic {name} mis à jour".format(name=obj), "success")
            else:
                flash("Pronostic {name} créé".format(name=obj), "success")
            return redirect(back)
        except PeeweeException as e:
            flash("Erreur lors de la sauvegarde : {e}".format(e=e), "error")
    elif form.is_submitted():
        flash("Erreurs à corriger dans le formulaire", "error")
    return render_template("forecast_form.html", object=obj, form=form, back=back)
