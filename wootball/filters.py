"""
Filtres Jinja
"""
from datetime import datetime

import premailer
from markdown import markdown
from markupsafe import Markup
from slugify import slugify


def premailer_transform(html, base_url):
    "premailer sur template html"
    return premailer.transform(html, base_url=base_url)


def strftime_filter(value, fmt: str) -> str:
    "formattage de datetime avec strftime"
    try:
        date = datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')
        return date.strftime(fmt)
    except TypeError:  # déjà une date
        return value.strftime(fmt)


def signed_number(number: int) -> str:
    "ajoute + sur number >= 0"
    try:
        return '+' + str(number) if number >= 0 else str(number)
    except TypeError:
        return '-'


def markdown_filter(text: str) -> str:
    "transforme du markdown en html"
    return Markup(markdown(text, extensions=['markdown.extensions.nl2br']))


def slugify_filter(text: str) -> str:
    "slugify pour jinja"
    return slugify(text)
