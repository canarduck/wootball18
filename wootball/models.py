"""Modèles pour wootball."""
import os
from datetime import datetime, timedelta
from hashlib import md5
from flask import url_for
from flask_security import PeeweeUserDatastore, RoleMixin, UserMixin
from flask_security.utils import get_identity_attributes
from pays import Countries
from peewee import (
    BooleanField,
    CharField,
    DateTimeField,
    ForeignKeyField,
    IntegerField,
    IntegrityError,
    ManyToManyField,
    TextField,
    fn,
)
from playhouse.flask_utils import FlaskDB
from slugify import slugify

from wootball.utils import thumbnail


class CustomFlaskDB(FlaskDB):
    def connect_db(self):
        if self.database.is_closed():
            self.database.connect()

    def close_db(self, exc):
        if not self.database.is_closed():
            self.database.close()


db_wrapper = FlaskDB()  # pylint: disable=C0103

SCORE_OUTCOME_OK = 3  # bonne prédiction gagnant
SCORE_OUTCOME_KO = -1  # mauvaise prédiction gagnant
SCORE_GOALS_OK = 1  # bonne prédiction nombre buts
SCORE_GOALS_DELTA = 2  # marge d'erreur avant fail sur prédiction buts
SCORE_GOALS_FAIL = -1  # si delta > sur prédiction nombre buts
SCORE_ANSWER_OK = 2  # bonne réponse question bonus
SCORE_ANSWER_KO = -1  # erreur question bonus

DEPARTMENT_CHOICES = [
    ("01", "Ain"),
    ("02", "Aisne"),
    ("03", "Allier"),
    ("04", "Alpes-de-Haute-Provence"),
    ("05", "Hautes-Alpes"),
    ("06", "Alpes-Maritimes"),
    ("07", "Ardèche"),
    ("08", "Ardennes"),
    ("09", "Ariège"),
    ("10", "Aube"),
    ("11", "Aude"),
    ("12", "Aveyron"),
    ("13", "Bouches-du-Rhône"),
    ("14", "Calvados"),
    ("15", "Cantal"),
    ("16", "Charente"),
    ("17", "Charente-Maritime"),
    ("18", "Cher"),
    ("19", "Corrèze"),
    ("2A", "Corse-du-Sud"),
    ("2B", "Haute-Corse"),
    ("21", "Côte-d’Or"),
    ("22", "Côtes-d’Armor"),
    ("23", "Creuse"),
    ("24", "Dordogne"),
    ("25", "Doubs"),
    ("26", "Drôme"),
    ("27", "Eure"),
    ("28", "Eure-et-Loir"),
    ("29", "Finistère"),
    ("30", "Gard"),
    ("31", "Haute-Garonne"),
    ("32", "Gers"),
    ("33", "Gironde"),
    ("34", "Hérault"),
    ("35", "Ille-et-Vilaine"),
    ("36", "Indre"),
    ("37", "Indre-et-Loire"),
    ("38", "Isère"),
    ("39", "Jura"),
    ("40", "Landes"),
    ("41", "Loir-et-Cher"),
    ("42", "Loire"),
    ("43", "Haute-Loire"),
    ("44", "Loire-Atlantique"),
    ("45", "Loiret"),
    ("46", "Lot"),
    ("47", "Lot-et-Garonne"),
    ("48", "Lozère"),
    ("49", "Maine-et-Loire"),
    ("50", "Manche"),
    ("51", "Marne"),
    ("52", "Haute-Marne"),
    ("53", "Mayenne"),
    ("54", "Meurthe-et-Moselle"),
    ("55", "Meuse"),
    ("56", "Morbihan"),
    ("57", "Moselle"),
    ("58", "Nièvre"),
    ("59", "Nord"),
    ("60", "Oise"),
    ("61", "Orne"),
    ("62", "Pas-de-Calais"),
    ("63", "Puy-de-Dôme"),
    ("64", "Pyrénées-Atlantiques"),
    ("65", "Hautes-Pyrénées"),
    ("66", "Pyrénées-Orientales"),
    ("67", "Bas-Rhin"),
    ("68", "Haut-Rhin"),
    ("69", "Rhône"),
    ("70", "Haute-Saône"),
    ("71", "Saône-et-Loire"),
    ("72", "Sarthe"),
    ("73", "Savoie"),
    ("74", "Haute-Savoie"),
    ("75", "Paris"),
    ("76", "Seine-Maritime"),
    ("77", "Seine-et-Marne"),
    ("78", "Yvelines"),
    ("79", "Deux-Sèvres"),
    ("80", "Somme"),
    ("81", "Tarn"),
    ("82", "Tarn-et-Garonne"),
    ("83", "Var"),
    ("84", "Vaucluse"),
    ("85", "Vendée"),
    ("86", "Vienne"),
    ("87", "Haute-Vienne"),
    ("88", "Vosges"),
    ("89", "Yonne"),
    ("90", "Territoire de Belfort"),
    ("91", "Essonne"),
    ("92", "Hauts-de-Seine"),
    ("93", "Seine-Saint-Denis"),
    ("94", "Val-de-Marne"),
    ("95", "Val-d’Oise"),
    ("971", "Guadeloupe"),
    ("972", "Martinique"),
    ("973", "Guyane"),
    ("974", "La Réunion"),
    ("976", "Mayotte"),
]

SHOE_SIZE_CHOICES = [(size, str(size)) for size in range(30, 49)]

# yapf: disable
PHASE_CHOICES = [
    ('pools', 'match de poule'),
    ('eights', 'huitièmes de finale'),
    ('quarters', 'quarts de finale'),
    ('semis', 'demi-Finales'),
    ('finals', 'Finale')
]
# yapf: enable

GROUP_CHOICES = [
    ("A", "A"),
    ("B", "B"),
    ("C", "C"),
    ("D", "D"),
    ("E", "E"),
    ("F", "F"),
    ("G", "G"),
    ("H", "H"),
]

WINNER_CHOICES = [("home", "Domicile"), ("away", "Déplacement")]


def countries_cca3():
    "Liste de pays sous la forme (cca3, nom)"
    return [(c.cca3, c.name) for c in Countries()]


class BaseModel(db_wrapper.Model):
    def refresh(self):
        "Actualisation de l’instance"
        return type(self).get(self._pk_expr())


class Competition(BaseModel):
    name = CharField(verbose_name="Nom", unique=True)
    external_id = IntegerField(verbose_name="ID sur football-data", null=True)

    def __str__(self) -> str:
        "name"
        return self.name


class Team(BaseModel):
    name = CharField(verbose_name="Nom", unique=True)
    slug = CharField(verbose_name="Slug", unique=True)
    country = CharField(verbose_name="Pays", max_length=3, default="FRA")
    external_id = IntegerField(
        verbose_name="ID sur football-data",
        null=True,
        help_text="Ne pas toucher, sauf si tu sais ce que c’est",
    )
    flag = CharField(
        verbose_name="Drapeau", null=True, help_text="Utiliser emojipedia.org si besoin"
    )
    group = CharField(
        verbose_name="Groupe", choices=GROUP_CHOICES, max_length=1, null=True
    )

    def save(self, force_insert=False, only=None):
        "enregistre slug"
        self.slug = slugify(str(self))
        return super().save(force_insert, only)

    def __str__(self) -> str:
        "Name (COUNTRY CODE)"
        return "{t.name} ({t.country})".format(t=self)

    @property
    def games(self):
        "matches joués par l’équipe"
        return Game.published().where(
            (Game.home_team == self.id) | (Game.away_team == self.id)
        )

    @property
    def avatar_url(self) -> str:
        "url de l’image de l’équipe par rapport à static"
        return "teams/{t.country}.png".format(t=self)

    @property
    def avatar_thumbnail_url(self) -> str:
        "url de l’image de l’équipe par rapport à static"
        return "thumbnails/teams/{t.country}.png".format(t=self)

    @classmethod
    def choices(cls) -> list:
        "Pour liste déroulante"
        return [(team.id, str(team)) for team in cls.select()]

    @property
    def get_absolute_url(self):
        "url détail façon django"
        return url_for("prono.team_detail", pk=self.id, slug=self.slug)

    @property
    def games_won(self):
        "décompte matches gagnés"
        home = (
            Game.doned()
            .where(
                (Game.home_team == self.id)
                & (Game.goals_home_team > Game.goals_away_team)
            )
            .count()
        )
        away = (
            Game.doned()
            .where(
                (Game.away_team == self.id)
                & (Game.goals_home_team < Game.goals_away_team)
            )
            .count()
        )
        return home + away

    @property
    def games_lost(self):
        "décompte matches perdus"
        home = (
            Game.doned()
            .where(
                (Game.home_team == self.id)
                & (Game.goals_home_team < Game.goals_away_team)
            )
            .count()
        )
        away = (
            Game.doned()
            .where(
                (Game.away_team == self.id)
                & (Game.goals_home_team > Game.goals_away_team)
            )
            .count()
        )
        return home + away

    @property
    def games_draw(self):
        "décompte matches nuls"
        return (
            Game.doned()
            .where(
                ((Game.home_team == self.id) | (Game.away_team == self.id))
                & (Game.goals_home_team == Game.goals_away_team)
            )
            .count()
        )


class Game(BaseModel):
    competition = ForeignKeyField(
        Competition, backref="games", on_delete="RESTRICT", on_update="CASCADE"
    )
    phase = CharField(verbose_name="Phase", choices=PHASE_CHOICES, default="pools")
    home_team = ForeignKeyField(
        Team,
        verbose_name="Équipe à domicile",
        backref="home_games",
        on_delete="RESTRICT",
        on_update="CASCADE",
    )
    away_team = ForeignKeyField(
        Team,
        verbose_name="Équipe en déplacement",
        backref="away_games",
        on_delete="RESTRICT",
        on_update="CASCADE",
    )
    goals_home_team = IntegerField(verbose_name="Buts à domicile", null=True)
    goals_away_team = IntegerField(verbose_name="Buts en déplacement", null=True)
    slug = CharField(verbose_name="Slug")
    date = DateTimeField(verbose_name="Date", default=datetime.now)
    draft = BooleanField(verbose_name="Brouillon", default=True)
    done = BooleanField(verbose_name="Finalisé", default=False)

    question = TextField(verbose_name="Question bonus", null=True)
    answer_choices = TextField(
        verbose_name="Réponses possibles",
        help_text="Choix séparés par des points-virgules",
        null=True,
    )
    answer = CharField(
        verbose_name="Réponse bonus",
        null=True,
        help_text="Doit faire partie des réponses possibles",
    )
    shoot_out_victory = CharField(
        verbose_name="Victoire tirs aux buts", choices=WINNER_CHOICES, null=True
    )

    created_at = DateTimeField(verbose_name="Créé le", default=datetime.now)
    updated_at = DateTimeField(verbose_name="MàJ le", null=True)
    scored_at = DateTimeField(verbose_name="Pointé le", null=True)
    external_id = IntegerField(verbose_name="ID sur football-data", null=True)

    def __str__(self) -> str:
        "home vs away (DD/MM)"
        return "{g.home_team.name} vs {g.away_team.name} ({g.date:%d/%m})".format(
            g=self
        )

    def save(self, force_insert=False, only=None):
        """
        * enregistre slug
        * vérifie intégrité question (si question, answers obligatoire)
        * vérifie que la réponse soit saisie si done = True et question
        * vérifie que la réponse soit dans la liste des réponses possibles
        """
        self.slug = slugify(str(self))
        self.updated_at = datetime.now()

        if self.question and not self.answer_choices_list:
            raise IntegrityError("Réponses possibles obligatoires si question")
        if self.question and self.done and not self.answer:
            raise IntegrityError("Réponse obligatoire avant done")
        if (
            self.done
            and self.answer_choices_list
            and self.answer not in self.answer_choices_list
        ):
            raise IntegrityError("Réponse n’est pas dans liste des réponses possibles")
        if self.done and self.draft:
            raise IntegrityError("Ne peut pas être en brouillon ET finalisé")
        if self.done and (self.goals_away_team is None or self.goals_home_team is None):
            raise IntegrityError("Ne peut être finalisé sans les buts")
        if self.home_team == self.away_team:
            raise IntegrityError("Match entre 2x la même équipe")
        if self.phase == "pools" and self.shoot_out_victory is not None:
            raise IntegrityError(
                "Ne peut être une victoire aux tirs aux buts & match de poules"
            )
        if self.shoot_out_victory and self.goals_home_team != self.goals_away_team:
            raise IntegrityError(
                "Ne peut être une victoire aux tirs aux buts si pas à égalité de buts"
            )
        if (
            self.done
            and self.phase != "pools"
            and self.goals_home_team == self.goals_away_team
            and self.shoot_out_victory is None
        ):
            raise IntegrityError(
                "Ne peut être une égalité but sans vainqueur au tirs aux buts pour un match hors poules"
            )
        return super().save(force_insert, only)

    def user_forecast(self, user: object) -> object:
        "Retourne le pronostic de `user` pour ce match ou False"
        try:
            return Forecast.get(user=user, game=self)
        except Forecast.DoesNotExist:
            return False

    @property
    def get_absolute_url(self):
        "url détail façon django"
        return url_for("prono.game_detail", slug=self.slug)

    @property
    def get_phase_display(self) -> str:
        "Phase de match en textuel"
        return dict(PHASE_CHOICES).get(self.phase)

    @property
    def closed(self):
        """
        Retourne False si ouvert aux pronostics, True si:
        * draft
        * done
        * now > date
        """
        if self.draft:
            return True
        if self.done:
            return True
        if self.date < datetime.now():
            return True
        return False

    @property
    def answer_choices_list(self) -> list:
        "'a;b ;c' -> ['a', 'b', 'c']"
        if not self.answer_choices or ";" not in self.answer_choices:
            return None
        answers = [a.strip() for a in str(self.answer_choices).split(";")]
        # suppression des réponses vides
        answers = list(filter(bool, answers))
        return answers if len(answers) > 1 else None

    @property
    def winner(self) -> str:
        """
        retourne home|away|None en fonction du résultat des péno OU des buts
        """
        if not self.done:
            return False
        if self.shoot_out_victory:
            return self.shoot_out_victory
        if self.goals_home_team == self.goals_away_team:
            return None
        if self.goals_home_team > self.goals_away_team:
            return "home"
        else:
            return "away"

    @property
    def winner_name(self) -> str:
        """
        retourne l’objet équipe qui a gagné ou 'match nul'
        """
        if not self.done:
            return False
        if self.winner == "home":
            return self.home_team
        if self.winner == "away":
            return self.away_team
        return "match nul"

    @property
    def score(self) -> int:
        "Points distribués lors des pronos"
        if not self.done:
            return None
        score = 0
        for forecast in self.forecasts:
            score += forecast.score

        return score

    @classmethod
    def forthcomings(cls) -> list:
        "Retourne les prochains (-2h -> futur) matchs publiés"
        return (
            cls.select()
            .where(cls.date > datetime.now() - timedelta(hours=2), cls.draft == False)
            .order_by(cls.date)
        )

    @classmethod
    def published(cls) -> list:
        "Retourne les matchs publiés"
        return cls.select().where(cls.draft == False).order_by(cls.date)

    @classmethod
    def doned(cls) -> list:
        "Retourne les matchs clôturés"
        return cls.select().where(cls.done == True).order_by(cls.date)


class Group(BaseModel):
    name = CharField(verbose_name="Nom", unique=True)
    slug = CharField(verbose_name="Slug")
    avatar = TextField(verbose_name="Avatar", null=True)
    description = TextField(verbose_name="Description", null=True)
    score = IntegerField(verbose_name="Score poules", default=0)
    score_finals = IntegerField(verbose_name="Score phase finale", default=0)
    created_at = DateTimeField(verbose_name="Inscrit le", default=datetime.now)
    updated_at = DateTimeField(verbose_name="MàJ le", null=True)

    def save(self, force_insert=False, only=None):
        "enregistre slug et met à jour le score"
        self.slug = slugify(str(self))
        self.updated_at = datetime.now()
        self.score = 0
        for user in self.users:
            try:
                self.score += user.score
            except TypeError:  # user.score is None
                continue
            try:
                self.score_finals += user.score_finals
            except TypeError:  # user.score_finals is None
                continue
        if self.avatar:
            try:
                thumbnail(self.avatar)
            except:
                self.avatar = None
        return super().save(force_insert, only)

    def __str__(self) -> str:
        "name"
        return self.name

    @property
    def avatar_path(self) -> str:
        "path de l’avatar par rapport à /"
        if not self.avatar:
            return None
        return os.path.join("wootball", "static", "uploads", self.avatar)

    @property
    def avatar_url(self) -> str:
        "url de l’avatar par rapport à static/"
        if not self.avatar:
            return "images/group-avatar.png"
        return "uploads/" + self.avatar

    @property
    def avatar_thumbnail_url(self) -> str:
        "url de l’avatar par rapport à static/"
        if not self.avatar:
            return "images/group-avatar.png"
        return "thumbnails/" + self.avatar

    @property
    def code(self) -> str:
        "code d’invitation"
        hashed = md5(self.slug.encode())
        return hashed.hexdigest()[:5]

    @property
    def get_absolute_url(self):
        "url détail façon django"
        return url_for("prono.group_detail", pk=self.id, slug=self.slug)

    @property
    def get_invitation_url(self):
        "url d’invitation (avec external pour être diffusée)"
        return url_for(
            "prono.group_invitation", pk=self.id, code=self.code, _external=True
        )

    @property
    def users_count(self):
        return self.users.count()

    @property
    def ranking(self) -> int:
        "classement"
        return Group.select().where(Group.score > self.score).count() + 1

    @property
    def shoe_size(self) -> float:
        "moyenne des pointures"
        avg = fn.AVG(User.shoe_size)
        query = User.select(avg).where(User.group == self)
        return float(query.scalar())

    @property
    def department(self) -> float:
        "moyenne des pointures"
        avg = fn.AVG(User.department)
        query = User.select(avg).where(User.group == self)
        return float(query.scalar())

    @property
    def likes_football(self) -> float:
        "% aimant le foot"
        likes = (
            User.select()
            .where((User.likes_football == True) & (User.group == self))
            .count()
        )
        percent = float(likes / User.select().where(User.group == self).count() * 100)
        return "{:.2f}".format(percent)


class Role(BaseModel, RoleMixin):
    name = CharField(verbose_name="Nom", unique=True)
    description = TextField(verbose_name="Description", null=True)

    def __str__(self) -> str:
        "name"
        return self.name


class User(BaseModel, UserMixin):
    # champs flask-security
    email = TextField(verbose_name="Email", unique=True)
    password = TextField(
        verbose_name="Mot de passe",
    )
    active = BooleanField(verbose_name="Activé", default=True)
    roles = ManyToManyField(Role, backref="users")
    confirmed_at = DateTimeField(verbose_name="Confirmé le", null=True)
    # extras
    username = CharField(verbose_name="Pseudo", unique=True)
    description = TextField(verbose_name="Description", null=True)
    slug = CharField(verbose_name="Slug")
    group = ForeignKeyField(
        Group, backref="users", null=True, on_delete="SET NULL", on_update="CASCADE"
    )
    created_at = DateTimeField(verbose_name="Inscrit le", default=datetime.now)
    updated_at = DateTimeField(verbose_name="MàJ le", null=True)
    avatar = TextField(verbose_name="Avatar", null=True)
    shoe_size = IntegerField(
        verbose_name="Pointure", choices=SHOE_SIZE_CHOICES, null=True
    )
    likes_football = BooleanField(verbose_name="J’aime le foot", default=True)
    department = CharField(
        verbose_name="Département", choices=DEPARTMENT_CHOICES, max_length=3, null=True
    )
    favorite = ForeignKeyField(
        Team,
        verbose_name="Équipe favorite",
        backref="supporters",
        null=True,
        on_delete="SET NULL",
        on_update="CASCADE",
    )
    score = IntegerField(verbose_name="Score poules", default=0)
    score_finals = IntegerField(verbose_name="Score phase finale", default=0)
    mailable = BooleanField(
        verbose_name="OK pour recevoir des mails liés aux pronostics", default=True
    )

    def save(self, force_insert=False, only=None):
        "enregistre slug"
        self.slug = slugify(str(self))
        self.updated_at = datetime.now()
        if self.avatar:
            try:
                thumbnail(self.avatar)
            except:
                self.avatar = None
        return super().save(force_insert, only)

    def update_score(self) -> int:
        "recalcule le score"
        if not self.forecasts.where(Forecast.scored_at.is_null(False)).count():
            return None
        score = 0
        score_finals = 0
        for forecast in self.forecasts:
            if forecast.scored_at:
                if forecast.game.phase == "pools":
                    score += forecast.score
                else:
                    score_finals += forecast.score
        if self.score != score or self.score_finals != score_finals:
            self.score = score
            self.score_finals = score_finals
            self.save()
            if self.group:  # update score groupe
                self.group.save()
        return score + score_finals

    def has_role(self, role):
        "test droit du user, ex. user.has_role('coach')"
        for r in self.roles:
            if str(r) == role:
                return True
        return False

    def __str__(self) -> str:
        "username"
        return self.username

    @property
    def code(self) -> str:
        "code désabonnement"
        hashed = md5(self.slug.encode())
        return hashed.hexdigest()[:5]

    @property
    def get_absolute_url(self):
        "url détail façon django"
        return url_for("prono.user_detail", pk=self.id, slug=self.slug)

    @property
    def ranking(self) -> int:
        "classement poules. None si aucun prono scoré"
        if not self.forecasts.where(Forecast.scored_at.is_null(False)).count():
            return None
        return User.select().where(User.score > self.score).count() + 1

    @property
    def ranking_finals(self) -> int:
        "classement phases finales. None si aucun prono scoré"
        if not self.forecasts.where(Forecast.scored_at.is_null(False)).count():
            return None
        return User.select().where(User.score_finals > self.score_finals).count() + 1

    @property
    def group_ranking(self) -> int:
        "classement"
        if not self.group:
            return False
        return (
            User.select()
            .where((User.score > self.score) & (User.group == self.group))
            .count()
            + 1
        )

    @property
    def avatar_path(self) -> str:
        "path de l’avatar par rapport à /"
        if not self.avatar:
            return None
        return os.path.join("wootball", "static", "uploads", self.avatar)

    @property
    def avatar_url(self) -> str:
        "url de l’avatar par rapport à static/"
        if not self.avatar:
            return "images/user-avatar.png"
        return "uploads/" + self.avatar

    @property
    def avatar_thumbnail_url(self) -> str:
        "url de l’avatar par rapport à static/"
        if not self.avatar:
            return "images/user-avatar.png"
        return "thumbnails/" + self.avatar

    @classmethod
    def stats_shoe_sizes(cls):
        "pointures (pointure, nombre)"
        query = (
            cls.select(cls.shoe_size, fn.Count(cls.id))
            .group_by(cls.shoe_size)
            .order_by(cls.shoe_size)
        )
        return query.tuples()

    @classmethod
    def stats_dep_scores(cls):
        "score par département (département, score cumulé)"
        query = (
            cls.select(cls.department, fn.SUM(cls.score))
            .group_by(cls.department)
            .order_by(cls.department)
        )
        return query.tuples()

    @classmethod
    def stats_likes(cls):
        "aime, aime pas"
        likes = cls.select().where(cls.likes_football == True).count()
        dislikes = cls.select().where(cls.likes_football == False).count()
        return (likes, dislikes)


class Forecast(BaseModel):
    game = ForeignKeyField(
        Game, backref="forecasts", on_delete="RESTRICT", on_update="CASCADE"
    )
    user = ForeignKeyField(
        User, backref="forecasts", on_delete="CASCADE", on_update="CASCADE"
    )
    created_at = DateTimeField(verbose_name="Créé le", default=datetime.now)
    updated_at = DateTimeField(verbose_name="MàJ le", null=True)
    scored_at = DateTimeField(verbose_name="Pointé le", null=True)
    notified_at = DateTimeField(verbose_name="Notifié le", null=True)
    goals_home_team = IntegerField(verbose_name="Buts équipe à domicile", default=0)
    goals_away_team = IntegerField(verbose_name="Buts équipe en déplacement", default=0)
    answer = CharField(verbose_name="Réponse", null=True)
    score_outcome = IntegerField(verbose_name="Points pour gagnant match", default=0)
    score_goals_home_team = IntegerField(verbose_name="Points buts domicile", default=0)
    score_goals_away_team = IntegerField(
        verbose_name="Points pour buts déplacement", default=0
    )
    score_answer = IntegerField(verbose_name="Points pour réponse", default=0)
    score = IntegerField(verbose_name="Total points", default=0)
    shoot_out_victory = CharField(
        verbose_name="Gagnant si tirs aux buts ?", choices=WINNER_CHOICES, null=True
    )

    def __str__(self) -> str:
        "home score vs score away"
        return "{f.game.home_team} {f.goals_home_team} vs {f.goals_away_team} {f.game.away_team}".format(
            f=self
        )

    def save(self, force_insert=False, only=None):
        """
        * enregistre date MAJ et vérifie intégrité du prono
        """
        self.updated_at = datetime.now()
        if self.game.phase == "pools" and self.shoot_out_victory is not None:
            raise IntegrityError(
                "Ne peut être une victoire aux tirs aux buts & match de poules"
            )
        if self.shoot_out_victory and self.goals_home_team != self.goals_away_team:
            raise IntegrityError(
                "Pour avoir une victoire aux tirs aux buts il faut d’abord une égalité (0-0, 1-1, etc.)"
            )
        if (
            self.game.phase != "pools"
            and self.goals_home_team == self.goals_away_team
            and self.shoot_out_victory is None
        ):
            raise IntegrityError(
                "Pour un match hors poules si égalité il faut un vainqueur aux tirs aux buts !"
            )
        return super().save(force_insert, only)

    def calculate_scores(self) -> int:
        """
        Calcul du résultat du pronostic
        gagnant = +SCORE_OUTCOME_OK/-SCORE_OUTCOME_KO
        buts home = +SCORE_GOALS_OK/-SCORE_GOALS_FAIL si delta > SCORE_GOALS_DELTA
        buts away = idem
        réponse question = +2/-1
        et sauvegarde du résultat
        """
        score = 0
        if not self.game.done:
            return 0
        if self.winner == self.game.winner:
            self.score_outcome = SCORE_OUTCOME_OK
        else:
            self.score_outcome = SCORE_OUTCOME_KO
        score += self.score_outcome
        if self.goals_home_team == self.game.goals_home_team:
            self.score_goals_home_team = SCORE_GOALS_OK
        elif abs(self.goals_home_team - self.game.goals_home_team) > SCORE_GOALS_DELTA:
            self.score_goals_home_team = SCORE_GOALS_FAIL
        score += self.score_goals_home_team
        if self.goals_away_team == self.game.goals_away_team:
            self.score_goals_away_team = SCORE_GOALS_OK
        elif abs(self.goals_away_team - self.game.goals_away_team) > SCORE_GOALS_DELTA:
            self.score_goals_away_team = SCORE_GOALS_FAIL
        score += self.score_goals_away_team
        if self.game.question:
            if self.answer and self.answer == self.game.answer:
                self.score_answer = SCORE_ANSWER_OK
            elif self.answer:
                self.score_answer = SCORE_ANSWER_KO
            score += self.score_answer
        else:
            self.score_answer = 0
        self.score = score
        self.scored_at = datetime.now()
        self.save()
        return score

    @property
    def winner(self) -> str:
        """
        retourne home|away|None en fonction des buts
        """
        if self.shoot_out_victory:
            return self.shoot_out_victory
        if self.goals_home_team == self.goals_away_team:
            return None
        if self.goals_home_team > self.goals_away_team:
            return "home"
        else:
            return "away"

    @property
    def winner_name(self) -> str:
        """
        retourne le nom du vainqueur fonction des buts
        """
        if self.winner == "home":
            return self.game.home_team
        if self.winner == "away":
            return self.game.away_team
        return "match nul"

    @property
    def locked(self) -> bool:
        """
        retourne true si pari est verrouillé
        * scoré
        * match commencé
        * match done
        """
        if self.scored_at:
            return True
        if self.game.date < datetime.now():
            return True
        if self.game.done:
            return True
        return False


class CustomPeeweeUserDatastore(PeeweeUserDatastore):
    """Surcharge de PeeweeUserDatastore pour adapter le get_user."""

    def get_user(self, identifier):
        """Un get_user qui ne passe que par les SECURITY_USER_IDENTITY_ATTRIBUTES.

        l'original fait une recherche par ID qui plante...
        """

        PeeweeUserDatastore
        from peewee import fn as peeweeFn

        for attr in get_identity_attributes():
            column = getattr(self.user_model, attr)
            try:
                return self.user_model.get(
                    peeweeFn.Lower(column) == peeweeFn.Lower(identifier)
                )
            except self.user_model.DoesNotExist:
                pass


user_datastore = CustomPeeweeUserDatastore(
    db_wrapper, User, Role, User.roles.get_through_model()
)

# from wootball.models import MODELS pour lister les tables
# utile pour initdb et autres
MODELS = (
    Role,
    User,
    User.roles.get_through_model(),
    Group,
    Competition,
    Team,
    Game,
    Forecast,
)
