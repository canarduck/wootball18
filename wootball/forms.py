"""Formulaires wootball.

* personalisation Flask-Security
* formulaires pronos
"""

from flask_security.forms import ConfirmRegisterForm as BaseConfirmRegisterForm
from flask_security.forms import Form as BaseForm
from flask_security.forms import RegisterForm as BaseRegisterForm
from flask_security.forms import (
    PasswordField,
    _datastore,
    current_user,
    email_required,
    email_validator,
    get_form_field_label,
)
from flask_security.utils import get_message, hash_password
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from wtforms.fields import BooleanField, SelectField, StringField, Field
from wtforms.fields.html5 import IntegerField, DateTimeField
from wtforms.validators import (
    InputRequired,
    NumberRange,
    Optional,
    ValidationError,
    Length,
    DataRequired,
)
from wtforms.widgets.html5 import EmailInput, NumberInput
from wtfpeewee.orm import model_form

from wootball.models import (
    DEPARTMENT_CHOICES,
    SHOE_SIZE_CHOICES,
    Forecast,
    Game,
    Group,
    Team,
    User,
)


def change_user_email(form, field: Field):
    """Vérifie que l'email envoyé est unique (si form.email != current_user)."""
    if _datastore.get_user(field.data) is not None and field.data != current_user.email:
        msg = get_message("EMAIL_ALREADY_ASSOCIATED", email=field.data)[0]
        raise ValidationError(msg)


def validate_antispam(form: BaseForm, field: Field):
    """Tentative de bloquer les spammeurs..."""
    if field.data.strip().lower() != "cantona":
        raise ValidationError(r"¯\_(ツ)_/¯ la réponse c’est : cantona")


UserProfileForm = model_form(
    User,
    exclude=[
        "email",
        "slug",
        "created_at",
        "updated_at",
        "score",
        "password",
        "active",
        "confirmed_at",
        "avatar",
    ],
    field_args={
        "email": dict(widget=EmailInput()),
        "shoe_size": dict(validators=[DataRequired()]),
        "description": dict(validators=[Length(max=500)]),
        "favorite": dict(validators=[DataRequired()]),
    },
)


class UserProfileMixin:
    """Infos complémentaires sur les utilisateurs."""

    username = StringField("Pseudo", [DataRequired()])
    shoe_size = SelectField(
        "Pointure",
        coerce=int,
        choices=[(0, "Je chausse du...")] + SHOE_SIZE_CHOICES,
        validators=[DataRequired()],
    )
    department = SelectField(
        "Département",
        choices=[(None, "J’habite dans...")] + DEPARTMENT_CHOICES,
        validators=[DataRequired()],
    )
    likes_football = BooleanField("J’aime le foot", default=True)
    mailable = BooleanField(
        "Recevoir par mail les résultats des pronostics", default=True
    )
    favorite = SelectField(
        "Équipe favorite", choices=[(None, "Équipe favorite")], validators=[]
    )


class UserEmailChangeFormMixin:
    email = StringField(
        get_form_field_label("email"),
        validators=[email_required, email_validator, change_user_email],
    )


class ConfirmRegisterForm(BaseConfirmRegisterForm, UserProfileForm):
    pass


class RegisterForm(ConfirmRegisterForm, BaseRegisterForm):
    antispam = StringField(
        "(Antispam) Quel est le nom de famille d’Éric Cantona",
        [InputRequired(), validate_antispam],
    )


class ProfileForm(BaseForm, UserEmailChangeFormMixin, UserProfileForm):
    new_avatar = FileField(
        "Avatar",
        validators=[
            FileAllowed(["jpg", "png", "gif"], "Doit être une image (png, jpg, gif)")
        ],
    )


BaseTeamForm = model_form(Team, exclude=["slug"])


class TeamForm(FlaskForm, BaseTeamForm):
    """
    Formulaire d'édition / création d'une équipe
    Hérite de
    * FlaskForm (pour les méthodes de traitement FlasK-WTF)
    * BaseTeamForm (pour la création des champs via wtfpeewee)
    """

    new_avatar = FileField(
        "Avatar",
        validators=[
            FileAllowed(["jpg", "png", "gif"], "Doit être une image (png, jpg, gif)")
        ],
    )


BaseGameForm = model_form(
    Game,
    exclude=["slug", "created_at", "updated_at", "scored_at"],
    field_args={
        "goals_home_team": dict(widget=NumberInput(min=0)),
        "goals_away_team": dict(widget=NumberInput(min=0)),
    },
)


class GameForm(FlaskForm, BaseGameForm):
    """
    Formulaire d'édition / création d'un match
    Hérite de
    * FlaskForm (pour les méthodes de traitement FlasK-WTF)
    * BaseGameForm (pour la création des champs via wtfpeewee)
    """

    def validate_answer_choices(self, field):
        "erreur si non vide mais sans ;"
        if field.data and ";" not in field.data:
            raise ValidationError("Doit contenir au moins un point virgule")


BaseUserForm = model_form(
    User,
    exclude=[
        "slug",
        "created_at",
        "updated_at",
        "score",
        "password",
        "avatar",
        "score_finals",
        "confirmed_at",
    ],
    field_args={
        "email": dict(widget=EmailInput()),
    },
)


class UserForm(FlaskForm, BaseUserForm, UserProfileMixin):
    """Formulaire d'édition / création d'un user."""

    email = StringField(
        get_form_field_label("email"), validators=[email_required, email_validator]
    )
    avatar = None
    change_password = PasswordField(
        "Changer mot de passe",
        description="si renseigné remplace le mot de passe actuel",
    )
    change_avatar = FileField(
        "Avatar",
        description="si renseigné remplace l’avatar actuel",
        validators=[
            FileAllowed(["jpg", "png", "gif"], "Doit être une image (png, jpg, gif)")
        ],
    )

    def new_password_hash(self):
        "retourne le mot de passe chiffré si password_new est set"
        if self.change_password.data:
            return hash_password(self.change_password.data)
        return None


BaseForecastForm = model_form(
    Forecast,
    exclude=[
        "created_at",
        "updated_at",
        "scored_at",
        "score_outcome",
        "score_goals_home_team",
        "score_goals_away_team",
        "score_answer",
        "score",
    ],
    field_args={
        "goals_home_team": dict(widget=NumberInput(min=0)),
        "goals_away_team": dict(widget=NumberInput(min=0)),
    },
)


class ForecastForm(FlaskForm, BaseForecastForm):
    """
    Formulaire d'édition / création d'un prono
    """

    notified_at = DateTimeField("Notifié du résultat le", validators=[Optional()])
    scored_at = DateTimeField("Scoré le", validators=[Optional()])
    answer = SelectField(
        "Question", choices=[("", "Pas de réponse")], validators=[Optional()]
    )


class ForecastCardForm(FlaskForm, BaseForecastForm):
    """
    Formulaire d'édition / création d'un prono sur le front
    """

    goals_home_team = IntegerField(
        "Buts home", [InputRequired(), NumberRange(min=0)], widget=NumberInput(min=0)
    )
    goals_away_team = IntegerField(
        "Buts away", [InputRequired(), NumberRange(min=0)], widget=NumberInput(min=0)
    )
    answer = SelectField(
        "Question", choices=[("", "Pas de réponse")], validators=[Optional()]
    )


BaseGroupForm = model_form(Group, only=["name", "description"])


class GroupForm(FlaskForm, BaseGroupForm):
    "Groupe"
    new_avatar = FileField(
        "Avatar",
        validators=[
            FileAllowed(["jpg", "png", "gif"], "Doit être une image (png, jpg, gif)")
        ],
    )
