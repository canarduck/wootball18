"""
jeux wootball

"""
from datetime import datetime
import re

import yaml
from flask import (Blueprint, abort, current_app, redirect, render_template,
                   request, send_file)

# pylint: disable=C0103
jeux = Blueprint('jeux', __name__)


@jeux.route('/')
def index():
    """
    Liste des jeux
    """
    return render_template('jeux.html')


@jeux.route('/schumacher-ne-serait-il-pas-un-gros-batard')
def quiz():
    """
    Questionnaire
    """
    return render_template('quiz.html')


@jeux.route('/schumacher-ne-serait-il-pas-un-gros-batard/crever')
def quiz_crever():
    """Tu peux crever..."""
    return render_template('quiz/crever.html')


@jeux.route('/schumacher-ne-serait-il-pas-un-gros-batard/felicitation')
def quiz_gagne():
    """gagné"""
    return render_template('quiz/gagne.html')


@jeux.route('/schumacher-ne-serait-il-pas-un-gros-batard/diplome')
def quiz_diplome():
    """Téléchargement diplôme"""
    from random import randint
    from io import BytesIO
    from PIL import Image, ImageDraw, ImageFont
    image = Image.open('wootball/static/images/diplome.png')
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype('wootball/static/fonts/CenturySchoolbook.ttf',
                              30)
    # nom
    name = request.args.get("name")[:20]
    draw.text((800, 684), name, font=font, fill=(0, 0, 0, 255))
    # numéro
    number = '14 - 2001 - {:03d}'.format(randint(1, 999))
    draw.text((1170, 810), number, font=font, fill=(0, 0, 0, 255))
    # date
    date = datetime.now().strftime('%d/%m/%Y')
    draw.text((940, 895), date, font=font, fill=(0, 0, 0, 255))
    byte_io = BytesIO()
    image.save(byte_io, 'PNG')
    byte_io.seek(0)
    return send_file(byte_io, mimetype='image/png')


@jeux.route('/schumacher-ne-serait-il-pas-un-gros-batard/minable')
def quiz_minable():
    """Mauvaise(s) réponse(s)"""
    return render_template('quiz/minable.html')


@jeux.route('/schumacher-ne-serait-il-pas-un-gros-batard/merde')
def quiz_merde():
    """merde"""
    return render_template('quiz/merde.html')


@jeux.route('/dezingue-schumacher')
def dezingue():
    """chamboule moi tout là dedans"""
    return render_template('dezingue.html')
