# Migrations

## Tirs aux buts

ALTER TABLE "game" ADD COLUMN "shoot_out_victory" VARCHAR(255);
ALTER TABLE "forecast" ADD COLUMN "shoot_out_victory" VARCHAR(255);

## Scores 2 phases

ALTER TABLE "user" ADD COLUMN "score_finals" INTEGER NOT NULL DEFAULT 0;
ALTER TABLE "group" ADD COLUMN "score_finals" INTEGER NOT NULL DEFAULT 0;