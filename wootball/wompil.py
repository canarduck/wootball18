"""
wompil wootball
"""
import random
import re

import yaml
from flask import Blueprint, abort, current_app, redirect, render_template

# pylint: disable=C0103
wompil = Blueprint('wompil', __name__)


@wompil.route('/<int:version>')
@wompil.route('/')
def detail(version: int = None):
    """
    Les infos sur les wompils viennent d'un fichier yaml
    pour des raisons de simplicité (pas envie d'utiliser la bdd)
    """
    current = version if version else current_app.config['CURRENT_WOMPIL']
    with open('wootball/yaml/wompils.yml', 'r') as stream:
        datas = yaml.safe_load(stream)
    try:
        data = datas[int(current)]
    except KeyError:
        abort(404)
    wompils = []
    for id_, infos in datas.items():
        wompils.append({
            'id': id_,
            'title': infos['title'],
            'short': infos['short']
        })
    return render_template('wompil.html',
                           current=int(current),
                           wompil=data,
                           wompils=wompils)


@wompil.app_template_filter('namebreak')
def namebreak(name):
    "Retourne le nom coupé par un <br>"
    return '<br>'.join(name.split(" ", 1))


@wompil.app_template_filter('linebreaks')
def linebreaks(text):
    "Convert newlines into <p> and <br>s."
    paras = re.split('\n{1,}', str(text))
    paras = [
        '<p class="content">%s</p>' % p.replace('\n', '<br>') for p in paras
    ]
    return '\n\n'.join(paras)


@wompil.app_template_filter('clubname')
def clubname(name):
    "Ajoute de quoi transformer le nom en club de foot"
    decoration = [
        'Le {name} FC', 'L’ASPTT {name}', 'L’AS {name}', 'Le SC {name}',
        'L’Olympique {name}', 'L’Étoile Sportive {name}',
        'L’Étoile Rouge {name}', 'Le Dynamo {name}', 'L’AEK {name}',
        'Le Spartak {name}', 'L’Espérance {name}', 'L’Ajax {name}',
        'L’Athlétic {name}', 'L’US {name}', 'Le CO {name}',
        'Le Deportivo {name}', 'L’OGC {name}', 'Le Real {name}',
        'Le {name} Olympique', 'Le {name} AC'
    ]
    return random.choice(decoration).format(name=name)
