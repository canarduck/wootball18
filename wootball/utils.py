"""
Utilitaires pour wootball
"""
import os
from random import randint
from PIL import Image, ImageOps
from werkzeug.utils import secure_filename
from slugify import slugify

AVATAR_SIZE = (256, 256)
THUMBNAIL_PATH = 'wootball/static/thumbnails'
UPLOAD_PATH = 'wootball/static/uploads'


def thumbnail(img: str) -> str:
    """
    Prend une image 
    * la redimensionne (taille + crop)
    """
    image = Image.open(os.path.join(UPLOAD_PATH, img))
    thumbnail = ImageOps.fit(image, AVATAR_SIZE)
    filename = '{path}/{name}'.format(path=THUMBNAIL_PATH, name=img)
    thumbnail.save(filename)
    return filename


def save_upload(upload: object, path: str, elements: list) -> str:
    """
    Prend un objet wtform File
    le sauvegarde dans `path` sous le nom elements[0]elements[n].extension
    retourne le nom
    """
    if hasattr(upload, 'filename'):
        _, ext = os.path.splitext(upload.filename)
        # rustine pour palier au cache du navigateur
        elements.append(str(randint(0, 1000)))
        filename = secure_filename('_'.join(elements) + ext)
        upload.save(os.path.join(UPLOAD_PATH, path, filename))
        return os.path.join(path, filename)
    return None