{{#general_title}}
# Journal des modifications

Ce fichier est généré automatiquement après chaque modification du dépôt. 
Les numéros indiqués correspondent aux [versions taggées](https://gitlab.com/canarduck/wootball/tags).

{{/general_title}}
{{#versions}}
## {{{label}}}

{{#sections}}
### {{{label}}}

{{#commits}}
* {{{subject}}}
{{/commits}}

{{/sections}}
{{/versions}}
