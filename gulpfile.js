var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var sass = require('gulp-sass');
var copy = require('gulp-copy');
var browserSync = require('browser-sync');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var postcss = require('gulp-postcss');
var uncss = require('postcss-uncss');
var minify = require('gulp-minify');
var concat = require('gulp-concat');
var reload = browserSync.reload;
var exec = require('child_process').exec;

var paths = {
    scss: ['wootball/sass/*.scss'],
    css: ['wootball/static/css/*.css', '!wootball/static/css/*.min.css'],
    js: ['wootball/js/*.js']
};

// liste d'urls pour uncss
var pages = [
    'http://localhost:3000',
]

gulp.task('scss', function () {
    return gulp.src(paths.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulp.dest('wootball/static/css'));
});

// uncss + minify
gulp.task('minify-css', function () {

    var plugins = [
        uncss({
            html: pages
        }),
    ];
    return gulp.src(paths.css)
        .pipe(postcss(plugins))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest('wootball/static/css'))
        .pipe(reload({
            stream: true
        }));
});


gulp.task('minify-js', function () {
    return gulp.src([
            'node_modules/soundmanager2/script/soundmanager2-nodebug.js', // todo: nodebug
            'wootball/js/*.js'
        ])
        .pipe(concat('wootball.js'))
        .pipe(minify({
            ext: {
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('wootball/static/js'))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('copy-js', function () {
    return gulp
        .src(['node_modules/chart.js/dist/Chart.min.js', 'node_modules/image-map-resizer/js/imageMapResizer.min.js', 'node_modules/tablesort/dist/tablesort.min.js', 'node_modules/tablesort/dist/sorts/tablesort.date.min.js', 'node_modules/tablesort/dist/sorts/tablesort.number.min.js'])
        .pipe(gulp.dest('wootball/static/js'));
});


// non inclus dans `default`:
// mieux vaut lancer flask dans un autre shell
// pour avoir le stdout
gulp.task('flask', function () {
    var proc = exec('python dev.py');
});

gulp.task('browser-sync', function () {
    browserSync({
        notify: true,
        proxy: "127.0.0.1:5000"
    });
});

gulp.task('watch', function () {
    gulp.watch(paths.scss, ['scss']);
    gulp.watch(paths.css, ['minify-css']);
    gulp.watch(paths.js, ['minify-js']);
    gulp.watch(["wootball/templates/**/*.html", "wootball/static/**/*.js"]).on('change', reload);
});

gulp.task('default', ['watch', 'browser-sync']);