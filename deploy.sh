#!/bin/sh
echo "+---------------------------+"
echo "+                           +"
echo "+ Déploiement prod wootball +"
echo "+                           +"
echo "+---------------------------+"

echo 
echo "Version cible : $1"
echo "Environnement demandé : $2"
export FLASK_CONFIG=$2
echo "Environnement actif : $FLASK_CONFIG"

echo 
echo "# 1/6 Arrêt des services"
sudo systemctl stop wootball
#sudo systemctl stop wootball-huey

echo 
echo "# 2/6 MAJ du dépôt"
git checkout master
git fetch --all --tags

echo
echo "# 3/6 Checkout du tag $1"
git checkout tags/$1

echo 
echo "# 4/6 Mise à jour pip"
. ../venv/bin/activate
pip install --upgrade pip wheel
pip install --upgrade -r requirements.txt -r prod-requirements.txt

echo 
echo "# 5/6 Mise à jour base de données"
#./manage.py importshops

echo
echo "# 6/6 Redémarrage des services"
#sudo systemctl start wootball-huey
sudo systemctl start wootball