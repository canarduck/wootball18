# Journal des modifications

Ce fichier est généré automatiquement après chaque modification du dépôt. 
Les numéros indiqués correspondent aux [versions taggées](https://gitlab.com/canarduck/wootball/tags).

## v0.7.5 (2018-06-30)

### ⚔ Correctifs

* Affichage des scores dans l'admin.

## v0.7.4 (2018-06-30)

### ⚔ Correctifs

* Options profil dans dropdown navbar.
* Responsivité des listes.

## v0.7.3 (2018-06-29)

### ⚔ Correctifs

* Correction texte règlement.

## v0.7.0 (2018-06-29)

### ★ Nouveautés

* Modal règlement & lots.
* Notion victoire par péno.

### ⚔ Correctifs

* Update score avant vérif.
* Scrore & classement sur profil user.
* Double classement selon phase.
* Modification user dans l'admin.
* Victoire tirs aux buts email.
* Victoire tirs aux buts dans les formulaires.
* Victoire tirs aux buts et score phases finales dans les modèles.

## v0.6.2 (2018-06-24)

### ⚔ Correctifs

* Responsive team & classement.
* Responsive.
* Tableau responsif.
* Tableau responsif et lien match.
* Typo.

## v0.6.1 (2018-06-23)

### ★ Nouveautés

* Page dédié aux détails d'un match.

## v0.6.0 (2018-06-23)

### ⚔ Correctifs

* Erreurs javascript département.
* Points par département.
* Calcul victoires/defaites/nul équipe.
* Min goal widget à 0.
* Désactivation mail inscription huey.

## v0.5.2 (2018-06-18)

### ⚔ Correctifs

* Désactivation des fouttues tâches huey.

## v0.5.0 (2018-06-16)

### ★ Nouveautés

* Affichage classement des pronostiqueurs.

### ⚔ Correctifs

* Classement user sur page user.

## v0.4.3 (2018-06-16)

### ★ Nouveautés

* Tri des tables dans l'admin.

### ⚔ Correctifs

* Optimisation envoi email.

## v0.4.2 (2018-06-15)

### ⚔ Correctifs

* Calcul score groupe.

## v0.4.1 (2018-06-13)

### ★ Nouveautés

* Clip veik.
* -1 sur mauvais gagnant de match.

### ⚔ Correctifs

* Email notification résultats.
* Arrondi moyenne pointure & département.

## v0.4.0 (2018-06-13)

### ⚔ Correctifs

* Amélioration listes admin.
* Erreurs email notif résultats.
* Taches scoring & envoi de mail en synchrone.
* Amélioration affichage frm admin forecast.
* Désactivation huey, driver sqlite poolé.

## v0.3.0 (2018-06-10)

### ★ Nouveautés

* Mail résultat des matchs.
* Mail notification nouveau matches.
* Email rappel matches à pronostiquer.

## v0.2.2 (2018-06-10)

### ★ Nouveautés

* Désabonnement mailing.
* Checkbox mailable sur profile.

### ⚔ Correctifs

* Maj dépendances.
* Calcul score groupe quand user.score None.
* Affichage classement None.
* Ranking = None sans prono.

### ✓ Autres

* Correctifs divers et mise en place désabonnement mailings.

## v0.2.1 (2018-05-27)

### ★ Nouveautés

* Favicons / metadonnées.

## v0.2.0 (2018-05-26)

### ★ Nouveautés

* Dézingue responsivement schumarer.
* Installation image-map-resizer.

### ✍ Changements

* Maj paquets npm & install  image-map-resizer.

### ⚔ Correctifs

* Image dezingue & lien quiz -> desingue.
* Footer.

## v0.1.6 (2018-05-21)

### ✍ Changements

* Mise en écoute de tous les morceaux.

## v0.1.5 (2018-05-19)

### ⚔ Correctifs

* Playlist wompil10.
* Date release party caen & ajout clip top montagne.
* Template notification mot de passe modifié.

## v0.1.4 (2018-05-10)

### ⚔ Correctifs

* Message tarif précommande sur wompil.

## v0.1.3 (2018-05-10)

### ⚔ Correctifs

* Photos wompil 14.
* Mise en page erreurs.
* Couleur navbar admin.

## v0.1.2 (2018-05-08)

### ⚔ Correctifs

* Pochette wompil 18.

## v0.1.1 (2018-05-05)

### ⚔ Correctifs

* Crédits wompil & durées.

## v0.1.0 (2018-05-05)

### ★ Nouveautés

* Mise en session changement langue.

### ✍ Changements

* Positionnement caption sur home.

### ⚔ Correctifs

* Nettoyage js traduction.
* Typos & couleurs quiz.
* Nommage rubriques.

## v0.0.9 (2018-05-01)

### ★ Nouveautés

* Events sur wompil.
* Vidéo s&d&f.

### ✍ Changements

* Retour au fond vert.

## v0.0.8 (2018-05-01)

### ★ Nouveautés

* Questionnaires wompil 18.

## v0.0.7 (2018-04-30)

### ★ Nouveautés

* Portraits wompil 18.

### ✍ Changements

* Simplification card panino.

## v0.0.6 (2018-04-23)

### ★ Nouveautés

* Bios wompil '18 & morceaux '14.

### ✍ Changements

* Maj textes quiz.
* Upgrade pip & wheel sur prod.

## v0.0.5 (2018-04-23)

### ★ Nouveautés

* Activation précommandes.
* Fichiers audio wompil14.
* Filtre markdown pour les wompils.

## v0.0.3 (2018-04-09)

### ⚔ Correctifs

* Dates factories.
* Intégration pyquery / flask.
* Html5 time.

## v0.0.2 (2018-04-09)

### ★ Nouveautés

* Page en construction.
* Page landing.
* Maquette landing.
* Test frm profile.
* Test thumbnail.
* Dashboard admin.
* Admin match & calcul scores.
* Champ scored_at sur Game.
* Tâches huey.
* Init huey.
* Date notification sur prono.
* Notification wootix.
* Change password.
* Reset mot de passe.
* Mot de passe perdu.
* Avatars groupes & desc user.
* Admin groupes.
* Page dédiée groupe.
* Cjhamps complémentaires user & group.
* Score groupe.
* Base de fonctionnement des groupes.
* Page user.
* Affichage des images des équipes.
* Équipes customisés.
* Videos wompil.
* Champ avatar pour équipe.
* Champ "phase" sur game.
* Pages d'erreur.
* Card pronostic.
* Curseur ballon.
* Init admin pronostics.
* Admin users.
* Init admin.
* Fixtures.
* Wavatars.
* Édition profil.
* Page intermédiaire quand attente action mail.
* Template mail html & text brut.
* Babelex pour les traductions.
* Chart.js.
* Tests modèles.
* Modèles & fakers.
* Test requirements.
* Mise en forme fquiz & generateur diplomes.
* Quiz.
* Page login.
* Menu burger.
* Ww2wigolisation des noms.
* Init CI.
* Portrait artistes.
* Gestion changelog / tags.
* Homepage.
* Page wompil.
* Init tests.
* Base des modèles et logging.
* Init flask.

### ✍ Changements

* Ajout du service huey.
* Amélioration emails.
* Urls des avatars.
* Refacto logging.
* Pages d'erreur en centered.
* Séparation des rubriques en blueprint.

### ⚔ Correctifs

* Changelog.
* Setup.cfg.
* Miniatures avatar équipes.
* Coverage.
* Nettoyage stats inutiles.
* Validation params wompil.
* Dossier static thumbnails.
* SECURITY_PASSWORD_SALT pour gitlab-ci.
* Retour calc_score.
* Tests index pronos.
* Naive date times.
* Masquage pagination inutile.
* Pickle lambda en retirant sender de send_mail_task.
* Pickle envoi mail security.
* Test pages d'erreur.
* Specifiaction python 3.5.
* Desactivation wavatarisation.
* Affichage des matches en anon.
* Clarification choix quiz (drop caps)
* Ordereddict yaml avec oyaml.
* Pronostic à zéro.
* Cartes prono.
* Gestion des erreurs d'intégrité en base.
* Conversion date depuis csv.
* Flask flash messages.
* Test jeux.
* Playlist 18.
* Player mp3.
* Path databases.
* Restructuration des dossiers.
* Ci pages.

### ✓ Autres

* Init homepage.
* Nettoyage avant merge.
* Vanilla js.
* Passage à bulma.
* Typos.
* Update sources média.
* Nettoyage html.
* Kick.
* Url mp3.
* Couleur tableau.
* Images de fond.
* Readme.
* Init CI pour gitlab pages.
* Player mp3.
* Terrain & première formation.
* Init scss.
* Init npm / gulp.
* Réorganisation fichiers.

