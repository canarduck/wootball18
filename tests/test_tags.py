"""
Test des template tags
"""

import unittest
from datetime import datetime

from wootball import strftime_filter, signed_number, premailer_transform
from wootball.wompil import clubname, linebreaks, namebreak


class StrftimeFilterTestCase(unittest.TestCase):
    """
    Filtre de formattage de date
    """

    def test_str(self):
        "formatage de date en str"
        datestr = '2018-01-01T12:00:00Z'
        result = strftime_filter(datestr, '%Y/%m/%d')
        self.assertEqual('2018/01/01', result)

    def test_date(self):
        "formatage de date en str"
        dateobject = datetime(2018, 1, 1)
        result = strftime_filter(dateobject, '%Y/%m/%d')
        self.assertEqual('2018/01/01', result)


class SignedNumberTestCase(unittest.TestCase):
    """
    nombre signés
    """

    def test_zero(self):
        "+0"
        self.assertEqual(signed_number(0), '+0')

    def test_positive(self):
        "positif"
        self.assertEqual(signed_number(1), '+1')

    def test_negative(self):
        "négatif"
        self.assertEqual(signed_number(-1), '-1')


class NameBreakTestCase(unittest.TestCase):
    "Prénom<br>nom"

    def test_simple(self):
        "nom simple espace"
        result = namebreak('canard duck')
        self.assertEqual(result.count('<br>'), 1)

    def test_composé(self):
        "nom composé"
        result = namebreak('can ard duck')
        self.assertEqual(result.count('<br>'), 1)
        # split au premier espace
        self.assertEqual(result.count('ard duck'), 1)


class LineBreakTestCase(unittest.TestCase):
    "<p>para<br>test</p>"

    def test_simple(self):
        "paragraphe"
        result = linebreaks('canard duck')
        self.assertEqual(result.count('</p>'), 1)

    def test_para_breaks(self):
        "paragraphe & retours à la ligne"
        result = linebreaks("""canard duck
        test
        test""")
        self.assertEqual(result.count('</p>'), 3)


class ClubnameTestCase(unittest.TestCase):
    "asptt tests"

    def test_simple(self):
        "clubname"
        name = 'canarduck'
        result = clubname(name)
        self.assertIn(name, result)
        self.assertGreater(len(result), len(name))


class PremailerTestCase(unittest.TestCase):
    """
    inclusion css dans html pour mails
    """

    def test_style(self):
        "input != output"
        html = '<style>p.bold { font-weight: bold }</style><p class="bold">bijour</p>'
        self.assertIn('font-weight:bold', premailer_transform(html, ''))
