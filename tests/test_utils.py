# import os
# from PIL import Image, ImageOps
# from shutil import copyfile
# import unittest

# from tests.factories import UserFactory
# from wootball.utils import thumbnail, save_upload, THUMBNAIL_PATH, UPLOAD_PATH, AVATAR_SIZE

# WOOTIX_SOURCE = 'wootball/static/images/wootix.png'
# WOOTIX_UPLOAD = UPLOAD_PATH + '/wootix.png'
# WOOTIX_THUMBNAIL = THUMBNAIL_PATH + '/wootix.png'


# @unittest.skip('pb png')
# class TestThumbnail):
#     """
#     Mignature d'une image
#     """

#     def setUp(self):
#         copyfile(WOOTIX_SOURCE, WOOTIX_UPLOAD)

#     def tearDown(self):
#         try:
#             os.remove(WOOTIX_UPLOAD)
#             os.remove(WOOTIX_THUMBNAIL)
#         except FileNotFoundError:
#             pass

#     def test_filename(self):
#         "nom du fichier"
#         filename = thumbnail('wootix.png')
#         self.assertIn(THUMBNAIL_PATH, filename)
#         self.assertIn('wootix.png', filename)
#         self.assertTrue(os.path.isfile(filename))

#     def test_filesize(self):
#         "taille du fichier"
#         filename = thumbnail('wootix.png')
#         im = Image.open(filename)
#         self.assertEqual(im.size, AVATAR_SIZE)

#     def test_from_user(self):
#         "création du fichier via user"
#         self.assertFalse(os.path.isfile(WOOTIX_THUMBNAIL))
#         UserFactory(avatar='wootix.png')
#         self.assertTrue(os.path.isfile(WOOTIX_THUMBNAIL))
