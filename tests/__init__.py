"""
Tests wootball

* BaseTestCase est à utiliser pour instancier les tests flask simples
* BaseViewTestCase est à utiliser pour instancier les tests des vues

"""
# import os
# import unittest

# from flask import Response, url_for
# from pyquery import PyQuery

# from wootball import create_app
# from wootball.models import db_wrapper


# class ResponsePyQuery(Response):
#     """
#     Ajoute un propriété `dom` à Response pour interroger le dom
#     d'une réponse html avec pyquery
#     """
#     def pq(self) -> PyQuery:
#         return PyQuery(self.get_data())


# class BaseTestCase(unittest.TestCase):
#     """
#     Base des tests

#         ConfigTestCase(BaseTestCase):
#             def test_one(self):
#                 assert self.app ...
#     """
#     @classmethod
#     def setUpClass(cls):
#         """
#         Spécifie l'utilisation de la config testing
#         ajoute app et db en propriété des tests
#         """
#         os.environ['FLASK_CONFIG'] = 'testing'
#         cls.app = create_app()
#         from wootball.models import MODELS
#         for model in MODELS:
#             model.drop_table(safe=True, cascade=False)
#             model.create_table(safe=False)

#     @classmethod
#     def tearDownClass(cls):
#         """On casse tout"""
#         from wootball.models import MODELS
#         for model in MODELS:
#             model.drop_table(safe=True, cascade=False)


# class BaseViewTestCase(BaseTestCase):
#     """
#     Base des tests faisant appel aux vues

#         FrontendTestCase(BaseTestCase):
#             def test_one(self):
#                 assert self.client.get('/') ...
#     TODO:
#     * signaux pour capturer templates
#     """
#     @classmethod
#     def setUpClass(cls):
#         """
#         Donne accès
#         * au test_client de flask via self.client()*
#         * les response disposent d'une propriété html pour accéder au dom via pyquery
#         """
#         super().setUpClass()
#         cls.app.response_class = ResponsePyQuery
#         cls.client = cls.app.test_client()

#     def url_for(self, route: str, **kwargs) -> str:
#         "url_for sans avoir à faire with context"
#         with self.app.test_request_context():
#             return url_for(route, **kwargs)

#     def login(self, username: str = 'wootix', password: str = 'wootix'):
#         "connexion"
#         with self.app.test_client() as client:
#             return client.post('/bijour',
#                                data=dict(email=username, password=password),
#                                follow_redirects=True)

#     def logout(self):
#         "déconnexion"
#         with self.app.test_client() as client:
#             return client.get('/boujou', follow_redirects=True)
