"""Test des pages du frontend."""

from wootball import handle_500

from flask import url_for

import pytest


@pytest.mark.usefixtures("client_class")
class TestHomepage:
    """Page d’accueil."""

    def test_display(self):
        """status code"""
        response = self.client.get(url_for("homepage"))
        assert response.status_code == 200

    def test_elements(self):
        """page d’accueil contient les 3 blocs"""
        pq = self.client.get(url_for("homepage")).pq()
        assert len(pq("#div-wompils")) == 1
        assert len(pq("#div-jeux")) == 1
        assert len(pq("#div-pronostics")) == 1


@pytest.mark.usefixtures("client_class")
class TestPouce:
    """pouce."""

    def test_display(self):
        """pouce"""
        response = self.client.get("/pouce")
        assert response.status_code == 200


@pytest.mark.usefixtures("client_class")
class TestErrors:
    """Pages d’erreur."""

    def test_404(self):
        "erreur 404"
        response = self.client.get("___")
        assert response.status_code == 404

    def test_500(self, app):
        "erreur 500"
        app.add_url_rule("/500", "500", view_func=handle_500)
        response = self.client.get("/500")
        assert response.status_code == 500

    @pytest.mark.options(WTF_CSRF_ENABLED=True)
    def test_400(self):
        "erreur 400 sur csrf"
        # self.app.config['WTF_CSRF_ENABLED'] = True  # disabled quand testing
        data = {"username": "a", "password": "b", "csrf_token": "c"}
        response = self.client.post("/bijour", data=data)
        assert response.status_code == 400


@pytest.mark.usefixtures("client_class")
class TestProfile:
    """Edition profil."""

    def test_login_required(self):
        "login requis"
        response = self.client.get(url_for("profile"))
        assert response.status_code == 302

    def test_content(self, user):
        "contenu de la page"
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)
        pq = self.client.get(url_for("profile")).pq()
        assert pq("form").attr("name") == "user_profile_form"
        assert pq("input#email")
        assert pq("input#username")
        assert pq("textarea#description")
        assert pq("input#new_avatar")
        assert pq("select#favorite")
        assert pq("select#shoe_size")
        assert pq("select#department")
        assert pq("input#likes_football")
        # sur register mais pas sur edit
        assert not pq("input#antispam")

    def test_errors(self, user):
        "champs en erreur"
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)

        response = self.client.post(url_for("profile"), data={"username": ""})
        pq = response.pq()
        assert len(pq(".field.is-danger")) >= 1

    # @unittest.skip("pb validation")
    # def test_success(self):
    #     "modif ok (msg + changement bdd"
    #     self.login()
    #     data = {
    #         "username": self.user.username,
    #         "description": "wootball",
    #         "email": self.user.email,
    #         "favorite": self.user.favorite.id,
    #         "department": self.user.department,
    #         "shoe_size": self.user.shoe_size,
    #         "likes_football": self.user.likes_football,
    #     }
    #     self.client.post(self.url, data=data, follow_redirects=True).pq()
    #     # self.assertGreaterEqual(len(pq('.notification.is-success')), 1)
    #     with self.app.test_request_context():
    #         user = User.get(username="wootix")
    #         self.assertEqual(user.description, "wootball")


@pytest.mark.usefixtures("client_class")
class TestRegister:
    """Création de compte."""

    def test_antispam(self):
        pq = self.client.get("/viens").pq()
        assert pq("input#antispam")
