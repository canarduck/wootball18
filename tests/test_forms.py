import pytest

from wootball.forms import validate_antispam

from wtforms.validators import ValidationError
from wtforms.fields import Field
from flask_wtf import FlaskForm


def test_validate_antispam():
    field = Field()
    form = FlaskForm
    field.data = "platini"
    with pytest.raises(ValidationError):
        validate_antispam(form, field)
    field.data = " CaNtONa "
    try:
        validate_antispam(form, field)
    except ValidationError as exc:
        pytest.fail(f"Ne devrait pas échouer sur {field.data} : {exc}")
