"""
Test des pages des pronos
"""
import pytest
from datetime import datetime, timedelta

from flask import url_for

from wootball.models import Forecast, Group, User, Team, Game
from tests.factories import GameFactory, GroupFactory, GameQuestionFactory


@pytest.fixture
def games(db):
    with db:
        games = GameFactory.create_batch(5, draft=False)
    yield games
    with db:
        Forecast.delete().execute()
        Game.delete().execute()
        Group.delete().execute()
        User.delete().execute()
        Team.delete().execute()


@pytest.mark.usefixtures("client_class")
class TestIndex:
    def test_display(self, games):
        "affichage"
        response = self.client.get(url_for("prono.index"))
        assert response.status_code == 200
        pq = response.pq()
        assert pq("#section-games")
        assert pq("#section-teams")
        assert pq("#section-stats")

    def test_anon(self, games):
        "page vue en anonyme"
        pq = self.client.get(url_for("prono.index")).pq()
        assert pq("#notification-anon")
        assert pq("#button-login")
        assert not pq("#nav-profile")

    def test_logged(self, games, user):
        "page vue authentifié"
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)
        pq = self.client.get(url_for("prono.index")).pq()
        assert not pq("#notification-anon")
        assert pq("#nav-profile")
        assert pq("#link-profile-detail")
        assert pq("#image-profile")
        assert pq("#profile-username")
        assert "wootix" in pq("#profile-username").text()
        assert pq("#profile-score")
        assert pq("#profile-ranking")
        assert pq("#button-profile-form")
        assert pq("#button-logout")
        assert pq("#button-group-form")
        assert not pq("#button-group-detail")
        assert not pq("#profile-group-ranking")

    def test_group(self, games, user, db):
        "page vue authentifié avec groupe"
        with db:
            user.group = GroupFactory()
            user.save()
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)
        pq = self.client.get(url_for("prono.index")).pq()
        assert not pq("#button-group-form")
        assert pq("#button-group-detail")

    def test_cards(self, games):
        "cartes de prono (anon)"
        pq = self.client.get(url_for("prono.index")).pq()
        count = Game.published().count()
        assert count >= 1
        cards = pq.find("div.card-forecast")
        assert count == len(cards)

    def test_draft_games(self, games, db):
        "les matchs en brouillon n’apparaissent pas"
        with db:
            game = GameFactory(draft=True)
        pq = self.client.get(url_for("prono.index")).pq()
        assert not pq.find(f"#card-forecast-{game.id}")

    def test_past_games(self, games, db):
        "les matchs passés de 2h n’apparaissent pas"
        with db:
            game = GameFactory(
                draft=False, date=datetime.now() - timedelta(hours=2, minutes=1)
            )
        pq = self.client.get(url_for("prono.index")).pq()
        assert not pq.find(f"#card-forecast-{game.id}")

    def test_forms(self, games, user):
        "formulaires de prono"
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)
        pq = self.client.get(url_for("prono.index")).pq()
        count = Game.published().count()
        assert count >= 1
        forms = pq("form.card-forecast")
        assert count == len(forms)

    def test_form_question(self, games, user, db):
        "formulaires de prono avec question bonux"
        with db:
            game = GameQuestionFactory(draft=False)
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)
        pq = self.client.get(url_for("prono.index")).pq()
        form = pq.find(f"#form-forecast-{game.id}")
        assert len(form.find("select")) == 1

    def test_form_locked(self, games, user, db):
        "formulaire verrouillé"
        with db:
            game = GameFactory(
                draft=False, done=True, goals_home_team=1, goals_away_team=0
            )
        data = {"email": user.email, "password": user.username}
        self.client.post("/bijour", data=data)
        pq = self.client.get(url_for("prono.index")).pq()

        form = pq.find(f"#form-forecast-{game.id}")
        button = form("button")
        assert button.attr("disabled")
