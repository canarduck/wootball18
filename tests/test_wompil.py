import pytest
from flask import url_for


@pytest.mark.usefixtures("client_class")
class TestWompil:
    def test_display(self):
        "status code"
        response = self.client.get(url_for("wompil.detail"))
        assert response.status_code == 200

    def test_current(self, app):
        "wompil courante depuis config"
        pq = self.client.get(url_for("wompil.detail")).pq()
        assert app.config["CURRENT_WOMPIL"] in pq("title").text()

    def test_old(self):
        "wompil 2010"
        version = "2010"
        url = url_for("wompil.detail", version=version)
        pq = self.client.get(url).pq()
        assert version in pq("title").text()

    def test_invalid(self):
        "wompil 404"
        version = "404"
        url = url_for("wompil.detail", version=version)
        response = self.client.get(url)
        assert response.status_code == 404

    def test_links(self):
        "liens vers les wompils"
        pq = self.client.get(url_for("wompil.detail")).pq()
        menu = pq("#menu-wompils")
        assert len(menu.find("a.link-wompil")) >= 1
        assert len(menu.find("a.is-active")) == 1

    def test_buy(self):
        "liens achat"
        pq = self.client.get(url_for("wompil.detail")).pq()
        menu = pq("#buttons-buy")
        assert len(menu.find("a")) >= 1

    def test_tracklist(self):
        "table tracklist"
        pq = self.client.get(url_for("wompil.detail")).pq()
        table = pq("table.tracklist>tbody")
        assert len(table.find("tr")) >= 1

    def test_tracklist_details(self):
        "détails artistes pour chq track"
        pq = self.client.get(url_for("wompil.detail")).pq()
        table = pq("table.tracklist>tbody")
        tracks = len(table.find("tr"))
        assert len(pq.find("section.track")) == tracks
