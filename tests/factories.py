from datetime import datetime, timedelta
import factory
from factory import fuzzy
from factory_peewee import PeeweeModelFactory
from wootball import models
from wootball.models import countries_cca3
from pytz import timezone

countries = [c[0] for c in countries_cca3()]

tz = timezone('Europe/Paris')


class TeamFactory(PeeweeModelFactory):
    class Meta:
        model = models.Team

    name = factory.Sequence(lambda n: 'Équipe %d' % n)
    country = factory.Iterator(countries)
    external_id = factory.Sequence(lambda n: n)


class UserFactory(PeeweeModelFactory):
    class Meta:
        model = models.User

    email = factory.Faker('free_email', locale='fr')
    password = factory.Faker('password')
    confirmed_at = fuzzy.FuzzyNaiveDateTime(
        datetime.now() - timedelta(days=31),
        datetime.now() - timedelta(minutes=1))
    username = factory.Faker('user_name', locale='fr')
    shoe_size = fuzzy.FuzzyInteger(35, 49)
    department = fuzzy.FuzzyInteger(10, 75)
    description = factory.Faker('catch_phrase', locale='fr')
    favorite = factory.SubFactory(TeamFactory)
    active = True


class GroupFactory(PeeweeModelFactory):
    class Meta:
        model = models.Group

    name = factory.Sequence(lambda n: 'Groupe %d' % n)
    description = factory.Faker('catch_phrase', locale='fr')


class RoleFactory(PeeweeModelFactory):
    class Meta:
        model = models.Role

    name = factory.Sequence(lambda n: 'Role %d' % n)


class CompetitionFactory(PeeweeModelFactory):
    class Meta:
        model = models.Competition

    name = factory.Sequence(lambda n: 'Compétition %d' % n)
    external_id = factory.Sequence(lambda n: n)


class GameFactory(PeeweeModelFactory):
    class Meta:
        model = models.Game

    phase = 'pools'
    competition = factory.SubFactory(CompetitionFactory)
    external_id = factory.Sequence(lambda n: n)
    home_team = factory.SubFactory(TeamFactory)
    away_team = factory.SubFactory(TeamFactory)
    date = fuzzy.FuzzyNaiveDateTime(datetime.now() + timedelta(days=1),
                                    datetime.now() + timedelta(days=31))
    done = False
    draft = True
    #question = factory.Faker('catch_phrase', 'fr')
    #answer_choices = 'A; B; C'
    #answer = 'A'


class GameQuestionFactory(GameFactory):
    "GameFactory avec question / réponse"
    question = factory.Faker('catch_phrase', locale='fr')
    answer_choices = 'A; B; C'
    answer = 'A'


class GameDoneQuestionFactory(GameQuestionFactory):
    "GameFactory avec question / réponse et score"
    done = True
    draft = False
    goals_home_team = fuzzy.FuzzyInteger(0, 5)
    goals_away_team = fuzzy.FuzzyInteger(0, 5)


class GameDoneFactory(GameFactory):
    "GameFactory avec score"
    done = True
    draft = False
    goals_home_team = fuzzy.FuzzyInteger(0, 5)
    goals_away_team = fuzzy.FuzzyInteger(0, 5)


class GameShootoutFactory(GameFactory):
    "Match"
    phase = 'eights'
    done = True
    draft = False
    goals_home_team = 1
    goals_away_team = 1
    shout_out_victory = fuzzy.FuzzyChoice(['home', 'away'])


class ForecastFactory(PeeweeModelFactory):
    class Meta:
        model = models.Forecast

    game = factory.SubFactory(GameFactory)
    user = factory.SubFactory(UserFactory)
