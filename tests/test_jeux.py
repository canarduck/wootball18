# import magic
import pytest
from flask import url_for


@pytest.mark.usefixtures("client_class")
class TestJeux:
    """Pages dédiées aux jeux"""

    def test_index(self):
        "status code"
        response = self.client.get(url_for("jeux.index"))
        assert response.status_code == 200

    def test_quiz(self):
        "status code"
        response = self.client.get(url_for("jeux.quiz"))
        assert response.status_code == 200

    def test_merde(self):
        "status code"
        response = self.client.get(url_for("jeux.quiz_merde"))
        assert response.status_code == 200

    def test_crever(self):
        "status code"
        response = self.client.get(url_for("jeux.quiz_crever"))
        assert response.status_code == 200

    def test_gagne(self):
        "status code"
        response = self.client.get(url_for("jeux.quiz_gagne"))
        assert response.status_code == 200

    def test_minable(self):
        "status code"
        response = self.client.get(url_for("jeux.quiz_minable"))
        assert response.status_code == 200

    # @unittest.skip('pb png')
    # def test_diplome(self):
    #     "send file"
    #     response = self.client.get(url_for('jeux.quiz_diplome') + '?name=test')
    #     assert response.status_code == 200
    #     mime = magic.from_buffer(response.get_data(), mime=True)
    #     assert 'image/png', mime)
