"""
Test des pages du frontend
"""
import pytest
from peewee import IntegrityError
from slugify import slugify
from datetime import datetime, timedelta

from tests.factories import (
    CompetitionFactory,
    UserFactory,
    GameFactory,
    ForecastFactory,
    GroupFactory,
    TeamFactory,
    GameDoneFactory,
    GameShootoutFactory,
    RoleFactory,
    GameDoneQuestionFactory,
)
from wootball.models import (
    Game,
    User,
    Forecast,
    Group,
    SCORE_ANSWER_KO,
    SCORE_ANSWER_OK,
    SCORE_GOALS_DELTA,
    SCORE_GOALS_FAIL,
    SCORE_GOALS_OK,
    SCORE_OUTCOME_KO,
    SCORE_OUTCOME_OK,
)


class TestCompetition:
    def test_str(self, app):
        name = "Testicule"
        competition = CompetitionFactory(name=name)
        assert str(competition) == name


class TestUser:
    def test_str(self, app):
        username = "Testicule"
        user = UserFactory(username=username)
        assert username == str(user)

    def test_slug(self, app):
        "slugify"
        user = UserFactory()
        assert user.slug == slugify(user.username)

    def test_ranking_no_forecast(self, app):
        "classement si aucun prono = None"
        user = UserFactory()
        assert user.ranking is None

    def test_ranking_with_forecasts(self, app):
        "classement avec pronostics ok"
        User.delete().execute()  # pylint: disable=E1120
        Forecast.delete().execute()  # pylint: disable=E1120
        Game.delete().execute()  # pylint: disable=E1120
        game = GameFactory(goals_home_team=5, goals_away_team=1, done=True, draft=False)
        user1 = UserFactory()
        user2 = UserFactory()
        # TODO si user sans prono, donc score 0, user2 passe 3e dans classement (score négatif)
        # user3 = UserFactory()
        ForecastFactory(user=user1, game=game, goals_home_team=3, goals_away_team=0)
        ForecastFactory(user=user2, game=game, goals_home_team=0, goals_away_team=5)
        for forecast in Forecast.select():
            forecast.calculate_scores()
        for user in User.select():
            user.update_score()
        user1 = user1.refresh()
        user2 = user2.refresh()
        # user3 = user3.refresh()
        assert user1.ranking == 1
        assert user2.ranking == 2
        # self.assertIsNone(user3.ranking)

    def test_ranking_with_forecast_not_scored(self, app):
        "classement si aucun prono non scorés = None"
        User.delete().execute()  # pylint: disable=E1120
        Forecast.delete().execute()  # pylint: disable=E1120
        Game.delete().execute()  # pylint: disable=E1120
        game = GameFactory(goals_home_team=5, goals_away_team=1)
        user1 = UserFactory()
        user2 = UserFactory()
        ForecastFactory(user=user1, game=game, goals_home_team=3, goals_away_team=0)
        ForecastFactory(user=user2, game=game, goals_home_team=0, goals_away_team=5)
        for forecast in Forecast.select():
            forecast.calculate_scores()
        for user in User.select():
            user.update_score()
        user1 = user1.refresh()
        user2 = user2.refresh()
        assert user1.ranking is None
        assert user2.ranking is None

    def test_score_pools(self, app):
        "score prono poules"
        user = UserFactory()
        game = GameDoneFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(
            user=user, game=game, goals_home_team=5, goals_away_team=1
        )  # +5
        forecast.calculate_scores()
        user.update_score()
        assert user.score == 5
        assert user.score_finals == 0

    def test_score_finals(self, app):
        "score prono finals"
        user = UserFactory()
        game = GameShootoutFactory(
            goals_home_team=5, goals_away_team=5, shoot_out_victory="home"
        )
        forecast = ForecastFactory(
            user=user, game=game, goals_home_team=3, goals_away_team=4
        )  # -1
        forecast.calculate_scores()
        user.update_score()
        assert user.score == 0
        assert user.score_finals == -1


class TestGroup:
    def test_str(self, app):
        "__str__"
        name = "Testicule"
        group = GroupFactory(name=name)
        assert name == str(group)

    def test_slug(self, app):
        "slugify"
        group = GroupFactory()
        assert group.slug == slugify(group.name)

    def test_code(self, app):
        "code à 5 chars"
        group = GroupFactory()
        assert len(group.code) == 5

    def test_fail_thumbnail(self, app):
        "avatar à none si fail thumbnail"
        group = GroupFactory(avatar="/.png")
        group = Group.get(id=group.id)
        assert group.avatar is None

    def test_avatar_url(self, app):
        "avatar par défaut"
        group = GroupFactory()
        assert ".png" in group.avatar_url

    def test_avatar_path_none(self, app):
        "avatar path = none is avatar none"
        group = GroupFactory()
        assert group.avatar_path is None

    def test_avatar_thumbnail_url(self, app):
        "avatar thumbnail par défaut"
        group = GroupFactory()
        assert ".png" in group.avatar_thumbnail_url

    def test_invitation_url(self, app):
        "url contient code"
        group = GroupFactory()
        assert group.code in group.get_invitation_url

    # def test_ranking(self):
    #     "classement"
    #     Group.delete().execute()  # pylint: disable=E1120
    #     group1 = GroupFactory(score=10)
    #     group2 = GroupFactory(score=5)
    #     group3 = GroupFactory(score=1)
    #     self.assertEqual(group1.ranking, 1)
    #     self.assertEqual(group2.ranking, 2)
    #     self.assertEqual(group3.ranking, 3)

    def test_shoe_size(self, app):
        "pointures moyennes"
        group = GroupFactory()
        UserFactory(shoe_size=40, group=group)
        UserFactory(shoe_size=44, group=group)
        assert group.shoe_size == 42.0

    def test_department(self, app):
        "département moyen"
        group = GroupFactory()
        UserFactory(department=40, group=group)
        UserFactory(department=44, group=group)
        assert group.department == 42.0

    def test_likes(self, app):
        "like moyen"
        group = GroupFactory(score=10)
        UserFactory(likes_football=True, group=group)
        UserFactory(likes_football=False, group=group)
        assert group.likes_football == "50.00"


class TestRole:
    def test_str(self, app):
        "__str__"
        name = "Testicule"
        role = RoleFactory(name=name)
        assert name == str(role)


class TestTeam:
    def test_str(self, app):
        "__str__"
        name = "Testicule"
        team = TeamFactory(name=name)
        assert name in str(team)

    def test_slug(self, app):
        "slugify"
        team = TeamFactory()
        assert slugify(team.name) in team.slug
        assert slugify(team.country) in team.slug

    def test_games(self, app):
        "matches"
        team1 = TeamFactory()
        team2 = TeamFactory()
        team3 = TeamFactory()
        GameFactory(draft=False, home_team=team1, away_team=team2)
        GameFactory(draft=False, home_team=team1, away_team=team3)
        GameFactory(draft=False, home_team=team3, away_team=team2)
        GameFactory(draft=False, home_team=team2, away_team=team1)
        assert team1.games.count() == 3
        assert team2.games.count() == 3
        assert team3.games.count() == 2

    def test_count_victories(self, app):
        "décompte victoires"
        team1 = TeamFactory()  # 3 victoires, 2 nuls, 1 défaite
        team2 = TeamFactory()  # 3 défaite, 3 nuls 2 victoire
        team3 = TeamFactory()  # 1 défaite, 1 nul
        GameDoneFactory(
            home_team=team1, away_team=team2, goals_home_team=1, goals_away_team=0
        )
        GameDoneFactory(
            home_team=team1, away_team=team2, goals_home_team=2, goals_away_team=0
        )
        GameDoneFactory(
            home_team=team1, away_team=team2, goals_home_team=5, goals_away_team=1
        )
        GameDoneFactory(
            home_team=team1, away_team=team2, goals_home_team=5, goals_away_team=5
        )
        GameDoneFactory(
            home_team=team1, away_team=team2, goals_home_team=0, goals_away_team=0
        )
        GameDoneFactory(
            home_team=team1, away_team=team2, goals_home_team=1, goals_away_team=2
        )
        GameDoneFactory(
            home_team=team3, away_team=team2, goals_home_team=1, goals_away_team=2
        )
        GameDoneFactory(
            home_team=team3, away_team=team2, goals_home_team=3, goals_away_team=3
        )
        assert team1.games_won == 3
        assert team1.games_draw == 2
        assert team1.games_lost == 1
        assert team2.games_won == 2
        assert team2.games_draw == 3
        assert team2.games_lost == 3
        assert team3.games_lost == 1
        assert team3.games_draw == 1


class TestGame:
    def test_str(self, app):
        "__str__"
        home_team = TeamFactory()
        away_team = TeamFactory()
        game = GameDoneQuestionFactory(home_team=home_team, away_team=away_team)
        assert home_team.name in str(game)
        assert away_team.name in str(game)

    def test_slug(self, app):
        "slugify"
        game = GameFactory()
        assert slugify(str(game)) == game.slug

    def test_winner_not_done(self, app):
        "winner false quand !game.done"
        game = GameFactory(goals_home_team=5, goals_away_team=1)
        assert not game.winner

    def test_winner_nul(self, app):
        "winner None quand match nul"
        game = GameDoneQuestionFactory(goals_home_team=1, goals_away_team=1)
        assert game.winner is None

    def test_winner_home(self, app):
        "winner home quand home>away"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        assert game.winner == "home"
        assert str(game.winner_name) == str(game.home_team)

    def test_winner_away(self, app):
        "winner away quand home<away"
        game = GameDoneQuestionFactory(
            goals_home_team=5,
            goals_away_team=15,
        )
        assert game.winner == "away"
        assert str(game.winner_name) == str(game.away_team)

    def test_answer_choices_save(self, app):
        "impossible de sauver si question mais answers vide"
        with pytest.raises(IntegrityError):
            GameFactory(question="question", answer_choices=None)

    def test_cant_be_done_without_goals(self, app):
        "done ne peut être true si buts manquants"
        game = GameFactory.build(
            goals_home_team=None, goals_away_team=None, done=True, draft=False
        )
        with pytest.raises(IntegrityError):
            game.save()

    def test_cant_be_done_without_answer(self, app):
        "done ne peut être true si question sans réponse"
        game = GameFactory.build(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            done=True,
            draft=False,
        )

        with pytest.raises(IntegrityError):
            game.save()

    def test_cant_be_done_with_invalid_answer(self, app):
        "done ne peut être true si réponse hors choix de réponse"
        game = GameFactory.build(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            answer="c",
            done=True,
            draft=False,
        )
        with pytest.raises(IntegrityError):
            game.save()

    def test_done_ok(self, app):
        "done peut être activé si tout est ok"
        game = GameFactory(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            answer="a",
        )
        game.done = True
        game.draft = False
        assert game.save()

    def test_user_forecast(self, app):
        "forecast d’un user pour un match"
        game = GameFactory(draft=False)
        user = UserFactory()
        forecast = ForecastFactory(game=game, user=user)
        found = game.user_forecast(user)
        assert found.id == forecast.id
        wrong_user = UserFactory()
        assert not game.user_forecast(wrong_user)

    def test_closed(self, app):
        "game closed sous condition"
        game = GameFactory(draft=True)
        assert game.closed
        game = GameDoneQuestionFactory()
        assert game.closed
        game = GameFactory(draft=False, date=datetime.now() - timedelta(hours=1))
        assert game.closed
        game = GameFactory(draft=False)
        assert not game.closed

    def test_same_teams(self, app):
        "exception si home = away"
        team = TeamFactory()
        game = GameFactory.build(home_team=team, away_team=team)
        with pytest.raises(IntegrityError):
            game.save()

    def test_draft_n_done(self, app):
        "exception si draft & done"
        game = GameFactory.build(draft=True, done=True)
        with pytest.raises(IntegrityError):
            game.save()

    def test_forecast_scores(self, app):
        "points attribués aux paris d'un match"
        game1 = GameDoneFactory(goals_home_team=2, goals_away_team=0)
        ForecastFactory(game=game1, goals_home_team=2, goals_away_team=0)  # +5
        ForecastFactory(game=game1, goals_home_team=2, goals_away_team=0)  # +5
        ForecastFactory(game=game1, goals_home_team=0, goals_away_team=3)  # -2
        game2 = GameDoneFactory(goals_home_team=0, goals_away_team=0)
        ForecastFactory(game=game2, goals_home_team=2, goals_away_team=0)  # +0
        ForecastFactory(game=game2, goals_home_team=5, goals_away_team=0)  # -1
        ForecastFactory(game=game2, goals_home_team=1, goals_away_team=1)  # +3
        game3 = GameFactory(goals_home_team=2, goals_away_team=0)
        ForecastFactory(game=game3, goals_home_team=2, goals_away_team=0)  # +5
        game4 = GameShootoutFactory(
            goals_home_team=2, goals_away_team=2, shoot_out_victory="home"
        )
        ForecastFactory(
            game=game4, goals_home_team=2, goals_away_team=2, shoot_out_victory="home"
        )  # +5
        for forecast in Forecast.select():
            forecast.calculate_scores()
        assert game1.score == 8
        assert game2.score == 2
        assert game3.score is None
        assert game4.score == 5

    def test_shoutout_victory_pools(self, app):
        "pas de victoire aux tirs aux but si phase poules"
        game = GameDoneFactory.build(shoot_out_victory="home")
        with pytest.raises(IntegrityError):
            game.save()

    def test_shoutout_victory_none(self, app):
        "égalité sans victoire aux tirs aux but hors phase poules"
        game = GameShootoutFactory.build(
            shoot_out_victory=None, goals_home_team=5, goals_away_team=5
        )
        with pytest.raises(IntegrityError):
            game.save()

    def test_shoutout_victory_diff_goals(self, app):
        "victoire aux tirs aux but score différent"
        game = GameShootoutFactory.build(
            shoot_out_victory="home", goals_home_team=5, goals_away_team=10
        )
        with pytest.raises(IntegrityError):
            game.save()

    def test_shoutout_winner(self, app):
        "équipe victoire aux tirs aux buts = winner"
        game = GameShootoutFactory(shoot_out_victory="home")
        assert game.winner == "home"
        game = GameShootoutFactory(shoot_out_victory="away")
        assert game.winner == "away"


class TestForecast:
    def test_str(self, app):
        "__str__"
        home_team = TeamFactory()
        away_team = TeamFactory()
        game = GameDoneQuestionFactory(home_team=home_team, away_team=away_team)
        forecast = ForecastFactory(game=game, goals_home_team=3, goals_away_team=1)
        assert home_team.name in str(forecast)
        assert away_team.name in str(forecast)
        assert "3" in str(forecast)
        assert "1" in str(forecast)

    def test_winner_nul(self, app):
        "winner None quand match nul"
        forecast = ForecastFactory(goals_home_team=1, goals_away_team=1)
        assert forecast.winner is None

    def test_winner_home(self, app):
        "winner home quand home>away"
        forecast = ForecastFactory(goals_home_team=5, goals_away_team=1)
        assert forecast.winner == "home"

    def test_winner_away(self, app):
        "winner away quand home<away"
        forecast = ForecastFactory(goals_home_team=5, goals_away_team=15)
        assert forecast.winner == "away"

    def test_score_game_not_done(self, app):
        "calc_score 0 sur !game.done"
        game = GameFactory(goals_home_team=5, goals_away_team=1, done=False)
        forecast = ForecastFactory(game=game, goals_home_team=3, goals_away_team=0)
        assert forecast.calculate_scores() == 0

    def test_score_game_done(self, app):
        "calc_score >= 0 sur game.done"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=3, goals_away_team=0)
        assert forecast.calculate_scores() >= 0

    def test_score_outcome_ok(self, app):
        "score gagnant ok"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=3, goals_away_team=0)
        forecast.calculate_scores()
        assert forecast.score_outcome == SCORE_OUTCOME_OK

    def test_score_outcome_ko(self, app):
        "score gagnant ko"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=0, goals_away_team=0)
        forecast.calculate_scores()
        assert forecast.score_outcome == SCORE_OUTCOME_KO

    def test_score_home_goals_ok(self, app):
        "score buts home ok"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=5, goals_away_team=0)
        forecast.calculate_scores()
        assert forecast.score_goals_home_team == SCORE_GOALS_OK

    def test_score_home_goals_ko(self, app):
        "score buts home ko"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=6, goals_away_team=1)
        forecast.calculate_scores()
        assert forecast.score_goals_home_team == 0

    def test_score_home_goals_fail(self, app):
        "score buts home à l'ouest"
        good = 5
        bad1 = good + SCORE_GOALS_DELTA + 1
        bad2 = good - SCORE_GOALS_DELTA - 1
        game = GameDoneQuestionFactory(goals_home_team=good, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=bad1, goals_away_team=0)
        forecast.calculate_scores()
        assert forecast.score_goals_home_team == SCORE_GOALS_FAIL
        forecast.goals_home_team = bad2
        forecast.calculate_scores()
        assert forecast.score_goals_home_team == SCORE_GOALS_FAIL

    def test_score_away_goals_ok(self, app):
        "score buts away ok"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=4, goals_away_team=1)
        forecast.calculate_scores()
        assert forecast.score_goals_away_team == SCORE_GOALS_OK

    def test_score_away_goals_ko(self, app):
        "score buts away ko"
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=1)
        forecast = ForecastFactory(game=game, goals_home_team=5, goals_away_team=0)
        forecast.calculate_scores()
        assert forecast.score_goals_away_team == 0

    def test_score_away_goals_fail(self, app):
        "score buts away à l'ouest"
        good = 5
        bad1 = good + SCORE_GOALS_DELTA + 1
        bad2 = good - SCORE_GOALS_DELTA - 1
        game = GameDoneQuestionFactory(goals_home_team=5, goals_away_team=good)
        forecast = ForecastFactory(game=game, goals_home_team=0, goals_away_team=bad1)
        forecast.calculate_scores()
        assert forecast.score_goals_away_team == SCORE_GOALS_FAIL
        forecast.goals_away_team = bad2
        forecast.calculate_scores()
        assert forecast.score_goals_away_team == SCORE_GOALS_FAIL

    def test_score_question_none(self, app):
        "score question si pas de réponse"
        game = GameDoneQuestionFactory(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            answer="a",
        )
        forecast = ForecastFactory(game=game, goals_home_team=5, goals_away_team=0)
        forecast.calculate_scores()
        assert forecast.score_answer == 0

    def test_score_question_ok(self, app):
        "score question si bonne réponse"
        game = GameDoneQuestionFactory(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            answer="a",
        )
        forecast = ForecastFactory(
            game=game, goals_home_team=5, goals_away_team=0, answer="a"
        )
        forecast.calculate_scores()
        assert forecast.score_answer == SCORE_ANSWER_OK

    def test_score_question_ko(self, app):
        "score question si mauvaise réponse"
        game = GameDoneQuestionFactory(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            answer="a",
        )
        forecast = ForecastFactory(
            game=game, goals_home_team=5, goals_away_team=0, answer="b"
        )
        forecast.calculate_scores()
        assert forecast.score_answer == SCORE_ANSWER_KO

    def test_scores(self, app):
        "tests calcul de score"
        game = GameDoneQuestionFactory(
            goals_home_team=5,
            goals_away_team=1,
            question="test",
            answer_choices="a;b",
            answer="a",
        )
        # tout bon
        forecast = ForecastFactory(
            game=game, goals_home_team=5, goals_away_team=1, answer="a"
        )
        forecast.calculate_scores()
        assert (
            forecast.score
            == SCORE_OUTCOME_OK + SCORE_GOALS_OK + SCORE_GOALS_OK + SCORE_ANSWER_OK
        )

        # gagnant + goals home - goals away + question
        forecast = ForecastFactory(
            game=game, goals_home_team=5, goals_away_team=4, answer="a"
        )
        forecast.calculate_scores()
        assert (
            forecast.score
            == SCORE_OUTCOME_OK + SCORE_GOALS_OK + SCORE_GOALS_FAIL + SCORE_ANSWER_OK
        )
        # le pire
        forecast = ForecastFactory(
            game=game, goals_home_team=0, goals_away_team=5, answer="b"
        )
        forecast.calculate_scores()
        assert (
            forecast.score
            == SCORE_OUTCOME_KO + SCORE_GOALS_FAIL + SCORE_GOALS_FAIL + SCORE_ANSWER_KO
        )

        # question bonne mais gagnant faux
        forecast = ForecastFactory(
            game=game, goals_home_team=3, goals_away_team=3, answer="a"
        )
        forecast.calculate_scores()
        assert forecast.score == SCORE_OUTCOME_KO + SCORE_ANSWER_OK
