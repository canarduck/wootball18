import pytest
from wootball import create_app, db_wrapper
from flask import Response
from pyquery import PyQuery
from wootball.models import MODELS
from .factories import UserFactory
from flask_security.utils import hash_password


class ResponsePyQuery(Response):
    """
    Ajoute un propriété `dom` à Response pour interroger le dom
    d'une réponse html avec pyquery
    """

    def pq(self) -> PyQuery:
        return PyQuery(self.get_data())


@pytest.fixture
def app(db):
    app = create_app("testing")
    app.response_class = ResponsePyQuery
    with db:
        for model in MODELS:
            model.create_table(safe=False)
    yield app
    with db:
        for model in MODELS:
            model.drop_table(safe=True, cascade=False)


@pytest.fixture
def db():
    return db_wrapper.database


@pytest.fixture
def user(db):
    with db:
        user = UserFactory(
            email="test@wootball.fr",
            username="wootix",
            password=hash_password("wootix"),
        )
    yield user
    with db:
        user.delete_instance()
