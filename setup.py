"""
Setup

Usage :
* `python setup.py test -> unittests
"""

from setuptools import setup, find_packages

try:
    from pypandoc import convert
    README = convert('README.md', 'rst')
except ImportError:
    print('!!! pandoc manquant, la description sera en markdown')
    README = open('README.md').read()

with open('requirements.in') as requirements:
    REQUIRES = requirements.read().splitlines()

setup(
    name='wootball',
    version='0.9.4',  # managed by bumbversion
    description='Le foot c’est naze',
    author='Canarduck',
    author_email='renaud@canarduck.com',
    url='https://gitlab.com/canarduck/disques',
    keywords='search crawl',
    install_requires=REQUIRES,
    packages=find_packages(),
    include_package_data=True,
    long_description=README,
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Topic :: Communications :: Email',
        'Framework :: Flask',
    ],
    test_suite='tests')
