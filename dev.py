#!/usr/bin/env python3
"""
Lanceur pour le serveur de développement wootball

`python dev.py`
"""
import os
from wootball import create_app

os.environ['FLASK_CONFIG'] = 'dev'
# pylint: disable=C0103
app = create_app('dev')
app.run(threaded=False)
