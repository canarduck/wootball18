# wootball 18

Site dédié à la compilation wootball 2018

WIP mais disponible sur [wootball.fr](http://2018.wootball.fr)

## Documentation

Y en a pas

## Développement

Le développement se passe sur [GitLab](https://gitlab.com/canarduck/wootball18).

[![build status](https://gitlab.com/canarduck/wootball18/badges/master/build.svg)](https://gitlab.com/canarduck/wootball/commits/master) [![coverage report](https://gitlab.com/canarduck/wootball18/badges/master/coverage.svg)](https://canarduck.gitlab.io/wootball18)