"""
Lanceur wsgi pour l'application disqu.es

Par défaut on utilise la config de prod

`gunicorn -w 4 -b 127.0.0.1:4000 wsgi:app`
"""
from wootball import create_app

# pylint: disable=C0103
app = create_app('prod')
